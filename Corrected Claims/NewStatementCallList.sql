select s.PatientNumber
	,cast(s.fromdate as date) as DOS
	,pp.SubscriberName as GuarantorName
	,pp.SubscriberBirthdate as GuarantorBirthdate
	,substring(pp.SubscriberPhoneNo, 1, 3) + '-' + 
		substring(pp.SubscriberPhoneNo, 4, 3) + '-' + 
		substring(pp.SubscriberPhoneNo, 4, 3)
		as GuarantorPhoneNumber
	,p.Name as PatientName
	,p.Birthdate as PatientBirthdate
	,substring(p.PhoneNo1, 1, 3) + '-' + 
		substring(p.PhoneNo1, 4, 3) + '-' + 
		substring(p.PhoneNo1, 4, 3)
		as PatientPhoneNumber
from service s
left join PatientPayor pp
	on s.PatientNumber = pp.PatientNumber
	and pp.Type = 'G'
left join Patient p
	on s.PatientNumber = p.Number
where s.Comment like '%corrected%'
	and s.PostedPeriod >= '201809'
	and s.StatementNumber <> ''
group by s.PatientNumber
	,cast(s.fromdate as date)
	,p.Name
	,p.Birthdate
	,p.PhoneNo1
	,pp.SubscriberName
	,pp.SubscriberBirthdate
	,pp.SubscriberPhoneNo
order by s.PatientNumber asc