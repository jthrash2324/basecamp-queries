select *
into #tmpClaimNumbers
from (
	select sn.PatientNumber
		,sn.ClaimNumber as NewClaimNumber
		,so.ClaimNumber as OldClaimNumber
	from service sn
	left join Adjustment an
		on sn.Number = an.ServiceNumber
		and an.AdjustmentTypeCode = '019'
	join service so
		on sn.PatientNumber = so.PatientNumber
		and so.TransToSvcNum = ''
		and so.ClaimNumber <> sn.ClaimNumber
		and so.Comment not like '%corrected%'
		and so.ClaimNumber <> ''
	where sn.Comment like '%corrected%'
		and sn.PostedPeriod >= '201809'
		and an.ServiceNumber is null
		and sn.ClaimNumber <> ''
	group by sn.PatientNumber 
		,sn.ClaimNumber
		,so.ClaimNumber
) as x

select *
into #tmpClaimServiceCounts
from (
	select tcn.PatientNumber
		,tcn.NewClaimNumber
		,count(csn.ServiceNumber) as NewServiceCount
		,'' as OldClaimNumber
		,'' as OldServiceCount
	from #tmpClaimNumbers tcn
	join ClaimService csn
		on tcn.NewClaimNumber = csn.ClaimNumber
	join Service s
		on csn.ServiceNumber = s.Number
		and s.amount * s.Units > 0.01
	group by tcn.PatientNumber
		,tcn.NewClaimNumber
	--order by tcn.PatientNumber
	
	union
	
	select tcn.PatientNumber
		,'' as NewClaimNumber
		,'' as NewServiceCount
		,tcn.OldClaimNumber
		,count(cso.ServiceNumber) as OldServiceCount
	from #tmpClaimNumbers tcn
	join ClaimService cso
		on tcn.OldClaimNumber = cso.ClaimNumber
	join Service s
		on cso.ServiceNumber = s.Number
		and s.amount * s.Units > 0.01
	group by tcn.PatientNumber
		,tcn.OldClaimNumber
) as y

select *
into #tmpCombinedCounts
from (
	select tcsc.PatientNumber
		,max(tcsc.NewClaimNumber) as NewClaimNumber
		,sum(tcsc.NewServiceCount) as NewServiceCount
		,max(tcsc.OldClaimNumber) as OldClaimNumber
		,sum(tcsc.OldServiceCount) as OldServiceCount
	from #tmpClaimServiceCounts tcsc
	group by tcsc.PatientNumber
) as z

select *
from #tmpCombinedCounts tcc
where tcc.OldServiceCount > 1
	and tcc.NewServiceCount <> tcc.OldServiceCount
order by tcc.PatientNumber asc

drop table #tmpClaimNumbers
drop table #tmpClaimServiceCounts
drop table #tmpCombinedCounts