select s.PatientNumber
	,s.Number
	,s.ServiceItemCode
	,a.AdjustmentTypeCode
	,a.Amount as AdjAmount
	,a.EnteredDate as AdjEnteredDate
	,cr.PayorNumber
	,pr.Name
	,p.Amount as PayAmount
	,p.EnteredDate as PayEnteredDate
from service s
join adjustment a
	on s.Number = a.ServiceNumber
	and a.AdjustmentTypeCode = '019'
join payment p
	on s.Number = p.ServiceNumber
	and p.EnteredDate > a.EnteredDate
join CashReceipt cr
	on p.CashReceiptNumber = cr.Number
join Payor pr
	on cr.PayorNumber = pr.Number
where a.PostedPeriod >= '201809'
