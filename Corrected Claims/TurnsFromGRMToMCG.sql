select p.Number as GRMPatientNumber
	,p.AliasName as IHSIPatientNumber
	,s.Amount as StartingBalance
	,a.Amount as AmountTurnedToMCG
	,cast(s.fromdate as date) as DOS
	,s.UserText1 as ClientAccountNumber
from service s
join patient p
	on s.PatientNumber = p.Number
join EMSLOSTurnsToGRM elt
	on p.AliasName = replicate('0', 9 - LEN(rtrim(elt.PatientNumber))) + rtrim(elt.PatientNumber)
	and s.UserText1 = replicate('0', 9 - LEN(rtrim(elt.AliasName))) + rtrim(elt.AliasName)
join Adjustment a
	on s.Number = a.ServiceNumber
	and a.AdjustmentTypeCode = '836'
group by p.Number 
	,p.AliasName 
	,s.Amount 
	,cast(s.fromdate as date) 
	,s.UserText1 
	,a.Amount