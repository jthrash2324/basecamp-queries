--disable trigger [dbo].[Patient_UTrig] ON [dbo].[Patient]
--go

--select s.PatientNumber
--	,p.BillingStatusCode
--	,bsc.Description
--update p
----set p.BillingStatusCode = 'S'
--set p.BillingStatusCode = 'Y'
	--case
	--	when p.BillingStatusCode = 'N' then 'S'
	--	when p.BillingStatusCode = 'C' then 'Y'
	--	else p.BillingStatusCode end
	,p.LastUpdatedBy = 'T018'
	,p.LastUpdatedDate = getdate()
from service s
join patient p
	on s.PatientNumber = p.Number
join IHSI_BillingStatusCodes bsc
	on p.BillingStatusCode = bsc.Code
where s.PostedPeriod >= '201809'
	and s.comment like '%corrected%'
	--and p.BillingStatusCode = 'N'
	and p.BillingStatusCode = 'C'
--group by s.PatientNumber
--	,p.BillingStatusCode
--	,bsc.Description
go

--enable trigger [dbo].[Patient_UTrig] ON [dbo].[Patient]
--go

SELECT name, is_disabled FROM sys.triggers
where is_disabled = 1