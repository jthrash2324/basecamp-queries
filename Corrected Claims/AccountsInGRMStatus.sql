select cc.server_name
	,cc.database_name
	,cc.ClientDBPatientNumber
	,cc.clientdbpatientbalance as ClientDBBalanceByPatient
	,s.FromDate as DOS
	,s.EnteredDate as TurnDate
	,p.Number as GRMPatientNumber
	,pro.OrganizationName as GRMProvider
	,bbs.Balance as GRMBalance
	,s.ServiceStatusCode as GRMStatus
from CorrectedClaimsInGRM cc
left join patient p
	on cc.ClientDBPatientNumber = p.aliasname
left join service s
	on p.Number = s.PatientNumber
left join provider pro
	on s.ProviderNumber = pro.Number
join IHSI_BalanceByService bbs
	on s.Number = bbs.ServiceNumber
where cc.clientaccountnumber = s.UserText1
order by pro.OrganizationName asc
	,bbs.Balance asc


drop table CorrectedClaimsInGRM cc