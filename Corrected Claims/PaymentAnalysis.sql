/*
Notes:

Modifications:
4/3/2019 by Justin:
	-Added a section to output line items along with patient balances for Taylor
*/


--IF EXISTS (select * from SIVSystem.dbo.Company where DatabaseName = db_name() and User1 like 'E%')  --E = ED client type, otherwise run the Clinical code.
--begin

	create table #tmpStep1 (
		PatientNumber char(9)
		,InitialCorrectedClaimDate smalldatetime
		,PreviousPayments float
		,PreviousPaymentAdjustments float
		,FollowingPayments float
		,FollowingPaymentAdjustments float
		,ProviderNumber char(7)
	)

	insert into #tmpStep1 (
		PatientNumber
		,InitialCorrectedClaimDate
		,ProviderNumber
	)

		select s.PatientNumber
			,min(s.EnteredDate) 
			,s.ProviderNumber
		from service s
		where s.Comment like '%Corrected%'
			and s.PostedPeriod >= '201809'
			and s.FromDate < '2018-08-01'
		group by s.PatientNumber
			,s.ProviderNumber
	
	select *
	into #tmpPayments
	from (
		select 
			ts1.PatientNumber
			,ts1.InitialCorrectedClaimDate
			,s.Number as ServiceNumber
			,s.ServiceStatusCode
			,p.Number as PaymentNumber
			,p.Amount as PaymentAmount
			,p.EnteredDate as PaymentEnteredDate
			,case
				when s.EnteredDate < ts1.InitialCorrectedClaimDate then 'PriorService'
				else 'FollowingService'
			end as ServiceCategory
			,case
				when p.EnteredDate < ts1.InitialCorrectedClaimDate then 'PriorPayment'
				else 'FollowingPayment'
			end as PaymentCategory
		from #tmpStep1 ts1
		left join Service s
			on ts1.PatientNumber = s.PatientNumber
		left join Payment p
			on s.Number = p.ServiceNumber
		left join CashReceipt cr
			on cr.Number = p.CashReceiptNumber
		left join Payor pay
			on cr.PayorNumber = pay.Number
		where pay.TypeCode <> 'A'	--Remove patient payments
		group by 		ts1.PatientNumber
			,ts1.InitialCorrectedClaimDate
			,s.Number
			,s.ServiceStatusCode
			,p.Number
			,p.Amount
			,s.EnteredDate
			,p.EnteredDate
	) as x

	select *
	into #tmpPaymentAdjustments
	from (
		select 
			ts1.PatientNumber
			,ts1.InitialCorrectedClaimDate
			,s.Number as ServiceNumber
			,s.ServiceStatusCode
			,a.Number as AdjustmentNumber
			,a.Amount as AdjustmentAmount
			,a.EnteredDate as AdjustmentEnteredDate
			,case
				when s.EnteredDate < ts1.InitialCorrectedClaimDate then 'PriorService'
				else 'FollowingService'
			end as ServiceCategory
			,case
				when a.EnteredDate < ts1.InitialCorrectedClaimDate then 'PriorAdjustment'
				else 'FollowingAdjustment'
			end as AdjustmentCategory
		from #tmpStep1 ts1
		left join Service s
			on ts1.PatientNumber = s.PatientNumber
		left join Adjustment a
			on s.Number = a.ServiceNumber
			and a.TransType = 'P'
		left join Payment pay
			on a.PaymentNumber = pay.Number
		left join CashReceipt cr
			on pay.CashReceiptNumber = cr.Number
		left join Payor p
			on cr.PayorNumber = p.Number
		where p.TypeCode <> 'A'
		group by ts1.PatientNumber
			,ts1.InitialCorrectedClaimDate
			,s.Number
			,s.ServiceStatusCode
			,a.Number
			,a.Amount
			,s.EnteredDate
			,a.EnteredDate
	) as y

	select *
	into #tmpPaymentsSummed
	from (
		select
			ts1.PatientNumber
			,sum(
				case
					when tp.PaymentCategory = 'PriorPayment' then tp.PaymentAmount
					else 0
				end
				) as PriorPayments
			,sum(
				case
					when tp.PaymentCategory = 'FollowingPayment' then tp.PaymentAmount
					else 0
				end
				) as FollowingPayments
		from #tmpStep1 ts1
		left join #tmpPayments tp
			on ts1.PatientNumber = tp.PatientNumber
		group by ts1.PatientNumber
	) as z


	select *
	into #tmpPaymentAdjustmentsSummed
	from (
		select
			ts1.PatientNumber
			,sum(
				case
					when ta.AdjustmentCategory = 'PriorAdjustment' then ta.AdjustmentAmount
					else 0
				end
				) as PriorAdjustments
			,sum(
				case
					when ta.AdjustmentCategory = 'FollowingAdjustment' then ta.AdjustmentAmount
					else 0
				end
				) as FollowingAdjustments
		from #tmpStep1 ts1
		left join #tmpPaymentAdjustments ta
			on ts1.PatientNumber = ta.PatientNumber
		group by ts1.PatientNumber
	) as a

	select *
	into #tmpAlmostDone
	from (
		select ts1.PatientNumber
			,ts1.InitialCorrectedClaimDate
			,tps.PriorPayments
			,tpas.PriorAdjustments
			,tps.PriorPayments + tpas.PriorAdjustments as PriorNetPayments
			,tps.FollowingPayments
			,tpas.FollowingAdjustments
			,tps.FollowingPayments + tpas.FollowingAdjustments as FollowingNetPayments
			,case
				when s.PatientNumber is not null then 'PendingAdjustment'
				when (s2.PatientNumber is not null and tps.FollowingPayments + tpas.FollowingAdjustments <> 0) then 'PendingAdjustment'
				when (s2.PatientNumber is not null and tps.FollowingPayments + tpas.FollowingAdjustments = 0) then 'PendingPayment'
				else 'Complete'
				end as Status
			,bbp.Balance as PatientBalance
			,ts1.ProviderNumber
		from #tmpStep1 ts1
		left join #tmpPaymentsSummed tps
			on ts1.PatientNumber = tps.PatientNumber
		left join #tmpPaymentAdjustmentsSummed tpas
			on ts1.PatientNumber = tpas.PatientNumber
		left join Service s
			on ts1.PatientNumber = s.PatientNumber
			and s.EnteredDate < ts1.InitialCorrectedClaimDate
			and s.Amount * s.Units > 0.01
			and s.ServiceStatusCode <> 40 
		left join IHSI_BalanceByPatient bbp
			on ts1.PatientNumber = bbp.PatientNumber
		left join Service s2
			on ts1.PatientNumber = s2.PatientNumber
			and s2.EnteredDate >= ts1.InitialCorrectedClaimDate
			and s2.Amount * s2.Units > 0.01
			and s2.ServiceStatusCode <> 40
		group by ts1.PatientNumber
			,ts1.InitialCorrectedClaimDate
			,tps.PriorPayments
			,tpas.PriorAdjustments
			,tps.FollowingPayments
			,tpas.FollowingAdjustments
			,s.PatientNumber
			,s2.PatientNumber
			,bbp.Balance
			,ts1.ProviderNumber
	) as b	
	
	select *
	into #tmpV1Output
	from (
		select 
			tad.PatientNumber
			,tad.InitialCorrectedClaimDate
			,tad.PriorNetPayments as PriorPayments
			,Case
				when tad.status = 'complete' then cast(tad.FollowingNetPayments as nvarchar(max)) 
				when tad.status = 'PendingPayment' then CAST(tad.FollowingNetPayments as nvarchar(max))
				else 'Pending'
			end as FollowingPayments
			,tad.PriorNetPayments +
				Case
					when tad.status = 'complete' then tad.followingNetPayments
					else 0
				end
			as TotalPayments
			,tad.Status
			--,tad.PatientBalance
			,tad.ProviderNumber
		from #tmpAlmostDone tad
		--where tad.status = 'PendingPayment'
		--where tad.patientnumber = '000026702'
		--order by tad.PatientNumber asc
	)TV1O

	select *
	into #tmpNewLevel
	from (
		select tv.*
			,max(s.ServiceItemCode) as NewLOS
		from #tmpV1Output tv
		join service s
			on tv.PatientNumber = s.PatientNumber
			and s.Comment like '%corrected%'
			and s.ServiceItemCode in (	'99281'
					,'99282' 
					,'99283' 
					,'99284'
					,'99285'  
					,'99201'
					,'99202' 
					,'99203' 
					,'99204'
					,'99205'
					,'99211'
					,'99212' 
					,'99213' 
					,'99214'
					,'99215')
		left join Adjustment a
			on s.Number = a.ServiceNumber
			and a.AdjustmentTypeCode = '019'
		where a.ServiceNumber is null
		group by tv.PatientNumber
			,tv.InitialCorrectedClaimDate
			,tv.PriorPayments
			,tv.FollowingPayments
			,tv.TotalPayments
			,tv.Status
			,tv.ProviderNumber
	)TN

	select *
	into #tmpLevelComparison
	from (
		select tnl.PatientNumber
			,tnl.InitialCorrectedClaimDate
			,tnl.PriorPayments
			,tnl.FollowingPayments
			,tnl.TotalPayments
			,tnl.Status
			,tnl.NewLOS
			,min(s.ServiceItemCode) as OldLOS
			,case when tnl.NewLOS > min(s.ServiceItemCode) then 'Up' 
				when tnl.NewLOS < min(s.ServiceItemCode) then 'Down' 
				else 'Same'
			end as LOSChange
			,tnl.ProviderNumber
		from #tmpNewLevel tnl
		join service s
			on s.PatientNumber = tnl.PatientNumber
			and s.ServiceItemCode in (	'99281'
										,'99282' 
										,'99283' 
										,'99284'
										,'99285'  
										,'99201'
										,'99202' 
										,'99203' 
										,'99204'
										,'99205'
										,'99211'
										,'99212' 
										,'99213' 
										,'99214'
										,'99215')
			and tnl.NewLOS <> s.ServiceItemCode
		join adjustment a
			on s.Number = a.ServiceNumber
			and a.AdjustmentTypeCode = '019'
		group by tnl.PatientNumber
			,tnl.InitialCorrectedClaimDate
			,tnl.PriorPayments
			,tnl.FollowingPayments
			,tnl.TotalPayments
			,tnl.Status
			,tnl.NewLOS
			,tnl.ProviderNumber
	)TLC

	select *
	into #tmpLineItems
	from (
		select tlc.PatientNumber
			,tlc.InitialCorrectedClaimDate
			,tlc.PriorPayments
			,tlc.FollowingPayments
			,tlc.TotalPayments
			,tlc.Status
			,tlc.NewLOS
			,tlc.OldLOS
			,tlc.LOSChange
			,case when tlc.Status = 'PendingPayment' and max(p.ServiceNumber) is null then 'NoResponse'
				when tlc.Status = 'PendingPayment' and max(p.ServiceNumber) is not null then 'Denied'
				else tlc.Status end as DetailedStatus
			,tlc.ProviderNumber
			,pro.OrganizationName
		from #tmpLevelComparison tlc
		left join service s
			on tlc.PatientNumber = s.PatientNumber
		left join payment p
			on s.Number = p.ServiceNumber
			and p.EnteredDate >= tlc.InitialCorrectedClaimDate
		left join CashReceipt cr
			on p.CashReceiptNumber = cr.Number
			and cr.PayorNumber <> '0000001'
		left join Provider pro
			on tlc.ProviderNumber = pro.Number
		--where tlc.LOSChange <> 'Same'
		group by tlc.PatientNumber
			,tlc.InitialCorrectedClaimDate
			,tlc.PriorPayments
			,tlc.FollowingPayments
			,tlc.TotalPayments
			,tlc.Status
			,tlc.NewLOS
			,tlc.OldLOS
			,tlc.LOSChange
			--,p.ServiceNumber
			,tlc.ProviderNumber
			,pro.OrganizationName
	)TLI

	--select count(*) from #tmpLineItems --Stop here to display line items

	--select tli.*	--Balance review for Taylor added 4/3/2019
	--	,bbp.Balance as PtBalance
	--from #tmpLineItems tli
	--left join IHSI_BalanceByPatient bbp
	--	on tli.PatientNumber = bbp.PatientNumber	

	--select round(sum(case when FollowingPayments = 'Pending' then 0 else cast(FollowingPayments as float) end), 0) as TotalPayments
	--	,count(distinct PatientNumber) as Claims
	--	,sum(case when Status = 'Complete' then 1 else 0 end) as CompletedClaims
	--	,round(sum(case when FollowingPayments = 'Pending' then 0 else cast(FollowingPayments as float) end)
	--		/sum(case when Status = 'Complete' then 1 else 0 end), 0) as PaymentsPerCompletedClaim
	--	,round(sum(case when FollowingPayments = 'Pending' then 0 else cast(FollowingPayments as float) end)
	--		/count(distinct PatientNumber), 0) as PaymentsPerAllClaims
	--	,ProviderNumber
	--	,OrganizationName
	--from #tmpLineItems
	--group by ProviderNumber
	--	,OrganizationName

	--select count(distinct tli.patientnumber) as PtsWithNegativeBalances
	--	,sum(bbs.Balance) as AR
	--from #tmpLineItems tli
	--join service s
	--	on tli.PatientNumber = s.PatientNumber
	--	and s.ServiceStatusCode not in (40, 50)
	--join IHSI_BalanceByService bbs
	--	on s.Number = bbs.ServiceNumber

	select count(distinct tli.patientnumber) as PtsWithCreditBalances
		,sum(bbs.Balance) as Credits
	from #tmpLineItems tli
	join service s
		on tli.PatientNumber = s.PatientNumber
		and s.ServiceStatusCode in (50)
	join IHSI_BalanceByService bbs
		on s.Number = bbs.ServiceNumber

	drop table #tmpLineItems
	drop table #tmpLevelComparison
	drop table #tmpNewLevel
	drop table #tmpV1Output
	drop table #tmpStep1
	drop table #tmpPayments
	drop table #tmpPaymentAdjustments
	drop table #tmpPaymentsSummed
	drop table #tmpPaymentAdjustmentsSummed
	drop table #tmpAlmostDone



--end 

--else 

--begin
--	select '1' from service where 1 <> 1
--end