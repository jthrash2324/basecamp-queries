select snew.PatientNumber
	,snew.FromDate as NewDOS
	,sold.FromDate as OldDOS
	--,snew.EnteredByID
from service snew
join service sold
	on snew.PatientNumber = sold.PatientNumber
join Adjustment inval
	on sold.Number = inval.ServiceNumber
	and inval.AdjustmentTypeCode = '019'
	and sold.Comment not like '%corrected%'
left join Adjustment invalnew
	on snew.Number = invalnew.ServiceNumber
	and invalnew.AdjustmentTypeCode = '019'
where snew.Comment like '%corrected%'
	and snew.PostedPeriod >= '201809'
	and cast(snew.FromDate as date) <> cast(sold.FromDate as date)
	and invalnew.ServiceNumber is null
	--and invalnew.AdjustmentTypeCode <> '019'
group by snew.PatientNumber
	,snew.FromDate
	,sold.FromDate
	--,snew.EnteredByID