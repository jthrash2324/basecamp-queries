select s.PatientNumber as ClientDBPatientNumber
	,bbp.Balance as ClientDBPatientBalance
	,p.AliasName as ClientAccountNumber
from service s
join service s2
	on s.PatientNumber = s2.PatientNumber
join adjustment a
	on s2.Number = a.ServiceNumber
	and a.AdjustmentTypeCode = '045'
left join IHSI_BalanceByPatient bbp
	on s.PatientNumber = bbp.PatientNumber
join Patient p
	on s.PatientNumber = p.Number
where s.PostedPeriod >= '201809'
	and s.comment like '%corrected%'
group by s.PatientNumber
	,bbp.Balance
	,p.AliasName
order by bbp.Balance asc