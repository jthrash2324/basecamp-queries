IF EXISTS (select * from SIVSystem.dbo.Company where DatabaseName = db_name() and User1 like 'E%') --Only auto-adjust ED clients

begin

select *	--Gather services that have been turned to collections which had a corrected claim go out after September
into #tmpServiceNumber
from (
	select s1.Number
		,s1.PatientNumber
		,s1.ServiceStatusCode
		,cast(s1.fromdate as date) as DOS
	from service s1
	join service s2
		on s1.PatientNumber = s2.PatientNumber
		and s1.Number <> s2.Number
		and s2.Comment like '%corrected%'
		and s2.PostedPeriod >= '201809'
	join adjustment a
		on s1.Number = a.ServiceNumber
		and a.AdjustmentTypeCode = '080'

--where s1.PatientNumber = '000039471'	--Testing
--where s1.PatientNumber in ('001167356','001182866','001155511')

	group by s1.Number
		,s1.PatientNumber
		,s1.ServiceStatusCode
		,cast(s1.fromdate as date)
) as x

-----------------------------------------

select * --Calculate amounts needed for adjustment and new status codes needed
into #tmpAdjustments
from (
	select ts.Number as ServiceNumber
		,ts.PatientNumber
		,ts.DOS
		,sum(a.amount) as TotalInCollections
		,bbs.Balance
		,ts.ServiceStatusCode
		,count(a.number) as AdjCount
		,-1*sum(a.amount) + bbs.Balance as NewBalanceShoes
		,case
			when -1*sum(a.amount) + bbs.Balance < 0 and ts.ServiceStatusCode <> 50 then '50'
			when -1*sum(a.amount) + bbs.Balance = 0 and ts.ServiceStatusCode <> 40 then '40'
			when -1*sum(a.amount) + bbs.Balance > 0 and ts.ServiceStatusCode in ('40','50') then '23'
		end as NewStatusCode	
	from #tmpServiceNumber ts
	join adjustment a
		on ts.Number = a.ServiceNumber
		and a.AdjustmentTypeCode in ('080','081')
	join IHSI_BalanceByService bbs
		on ts.Number = bbs.ServiceNumber
	group by ts.Number
		,ts.PatientNumber
		,bbs.Balance
		,ts.ServiceStatusCode
		,ts.DOS
) as y

delete --Don't waste adjustment numbers
from #tmpAdjustments
where TotalInCollections = 0

alter table #tmpAdjustments
add RowID smallint identity
	,AdjNumber varchar(9)

--select * from #tmpAdjustments	--QA
-------------------------------------

--DECLARE @NextAdjustmentNumber int	--Run adjustments
--SET @NextAdjustmentNumber = (SELECT [NextNumber] FROM dbo.AdjustmentSequence)

--UPDATE #tmpAdjustments
--SET AdjNumber = RIGHT(RTRIM('000000000' + CAST((RowID) + @NextAdjustmentNumber AS VARCHAR)), 9)
	
--DECLARE @TransDate smalldatetime
--SET @TransDate = CAST(FLOOR(CAST(getdate() AS FLOAT))AS SMALLDATETIME)

--INSERT INTO dbo.Adjustment (AdjReasonCategory, AdjReasonCode, AdjustmentTypeCode, Amount, EnteredByID, 
--	EnteredDate, Number, PaymentNumber, PayorNumber, Posted, PostedPeriod, PrintOnStmtFlag, ReasonText, 
--	ServiceNumber, StatementNumber, SubAccount, TransType, UserDate1, UserDate2, UserFloat1, UserFloat2, UserText1, 
--	UserText2, UserText3, UserText4)
--SELECT '' AS AdjReasonCategory
--	,'' AS AdjReasonCode 
--	,'081' AS AdjustmentTypeCode
--	,-1*TotalInCollections AS Amount
--	,'T018' AS EnteredByID
--	,@TransDate AS EnteredDate
--	,AdjNumber AS Number
--	,'' AS PaymentNumber
--	,'' AS PayorNumber
--	,'Y' AS Posted
--	,(SELECT CurrentPeriodNumber FROM dbo.mbsetup) AS PostedPeriod
--	,'Y' AS PrintOnStmtFlag
--	,'CORRECTED CLAIM SENT, LOS' AS ReasonText
--	,ServiceNumber as ServiceNumber
--	,'' AS StatementNumber
--	,'' AS SubAccount
--	,'S' as TransType
--	,@TransDate AS UserDate1
--	,'1900-01-01 00:00:00' as UserDate2
--	,0 as UserFloat1
--	,0 as UserFloat2
--	,'' as UserText1
--	,'' as UserText2
--	,'' as UserText3
--	,'' as UserText4
	
--FROM #tmpAdjustments

--UPDATE [dbo].[AdjustmentSequence]
--SET [NextNumber] = (SELECT MAX(RIGHT(RTRIM('000000000' + Number), 9)) + 1 FROM Adjustment)


-------------------------------------

--update s	--Update service status codes
--set s.ServiceStatusCode = ta.NewStatusCode
--	,s.LastUpdatedBy = 'T018'
--	,s.LastUpdatedDate = getdate()
--from service s
--join #tmpAdjustments ta
--	on s.Number = ta.ServiceNumber
--where ISNUMERIC(ta.NewStatusCode) = 1

---------------------------------------


--select * from #tmpAdjustments	--QA
drop table #tmpServiceNumber	--Clean up temp tables
drop table #tmpAdjustments

end