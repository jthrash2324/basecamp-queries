select s.PatientNumber
	,s.Number
	,s.ServiceItemCode
	,s.Comment
	,s.PostedPeriod
	,p.EOBReasonText
	,s.ServiceStatusCode
	,an.ReasonText
from service s
join service so
	on so.PatientNumber = s.PatientNumber
	and so.Number <> s.Number
	and so.Comment not like '%correc%'
	and so.Comment not like '%ICN%'
join Adjustment ao
	on ao.ServiceNumber = so.Number
	and ao.AdjustmentTypeCode = '019'
left join IHSI_MGTPayments p
	on p.svcnumber = s.Number
	and p.PayAmount + p.AdjAmount = 0
	and ((p.EOBReasonText like '%29%'
		and p.EOBReasonText like '%CO%'
		and p.EOBReasonText not like '%129%')
		or p.EOBReasonText like '%tf%')
left join Adjustment an
	on s.Number = an.ServiceNumber
	and an.ReasonText like '%tf%'

where s.Comment like '%correc%'
	and s.PostedPeriod >= '201809'
	and s.PostedPeriod < '201812'
	and s.TransToSvcNum = ''
	and s.Comment not like '%icn%'
	and (p.svcnumber is not null or an.ServiceNumber is not null)
group by s.PatientNumber
	,s.Number
	,s.ServiceItemCode
	,s.Comment
	,s.PostedPeriod
	,p.EOBReasonText
	,s.ServiceStatusCode
	,an.ReasonText
order by s.PatientNumber asc
	,s.Number asc