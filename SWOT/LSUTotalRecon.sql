select *
into #tmpClientReport
from (
	select 
		lpt.MRN as ClientMRN
		,substring(lpt.Arrived,2,10) as ReportDOS
		,substring(lpt.Arrived,8,4) as ClientYOS
		,substring(lpt.Arrived,2,2) as ClientMOS
		,substring(lpt.Arrived,5,2) as ClientDayOS
		,case
			when SUBSTRING(lpt.arrived,5,4) = '2018' then 'N'
			else 'Y'
		end as DateCorrectlyFormatted
		,substring(lpt.Arrived,8,4)  + '-' + substring(lpt.Arrived,2,2) + '-' + substring(lpt.Arrived,5,2) as ParsedClientDOS
		--,convert(smalldatetime,substring(lpt.Arrived,8,4)  + '-' + substring(lpt.Arrived,2,2) + '-' + substring(lpt.Arrived,5,2)) as ClientDOS
		,lpt.Transferred as ClientTransfer
		,lpt.ED_Disposition as ClientEDDisposition
	from LANB2018PatCount lpt
	group by 
		lpt.MRN 
		,lpt.Arrived 
		,lpt.Transferred 
		,lpt.ED_Disposition 
) as A

select *
into #tmpMRNMatch
from (
	select tcr.ClientMRN
		,tcr.ReportDOS
		,tcr.DateCorrectlyFormatted
		,case
			when tcr.DateCorrectlyFormatted = 'Y' then cast(tcr.ParsedClientDOS as date)
			else null
			end as ParsedClientDOS
		,tcr.ClientTransfer
		,tcr.ClientEDDisposition
		,case
			when p.MedicalRecordNo is not null then 'Y'
			else 'N'
			end as MRNMatch
	from #tmpClientReport tcr
	left join patient p
		on tcr.ClientMRN = p.MedicalRecordNo
	group by tcr.ClientMRN
		,tcr.ReportDOS
		,tcr.DateCorrectlyFormatted
		,tcr.ParsedClientDOS
		,tcr.ClientTransfer
		,tcr.ClientEDDisposition
		,p.MedicalRecordNo
) as B

select *
into #tmpDOSMatch
from (
	Select tmm.ClientMRN
		,tmm.ReportDOS
		,tmm.DateCorrectlyFormatted
		,tmm.ParsedClientDOS
		,tmm.ClientTransfer
		,tmm.ClientEDDisposition
		,tmm.MRNMatch
		,DATEDIFF(day, tmm.ParsedClientDOS,cast(s.fromdate as date)) as DaysOff
		,cast(s.FromDate as date) as DOSClose
		,s.PatientNumber
		,p.AliasName as ClientAccountNumber
		,pro.OrganizationName
	from #tmpMRNMatch tmm
	left join Patient p
		on tmm.ClientMRN = p.MedicalRecordNo
	join service s
		on p.Number = s.PatientNumber
		and tmm.ParsedClientDOS >= dateadd(day,-1,cast(s.fromdate as date))
		and tmm.ParsedClientDOS <= dateadd(day,1,cast(s.fromdate as date))
		and s.TransToSvcNum  = ''
	join Provider pro
		on s.ProviderNumber = pro.Number
	group by tmm.ClientMRN
		,tmm.ReportDOS
		,tmm.DateCorrectlyFormatted
		,tmm.ParsedClientDOS
		,tmm.ClientTransfer
		,tmm.ClientEDDisposition
		,tmm.MRNMatch
		,cast(s.FromDate as date)
		,s.PatientNumber
		,p.AliasName
		,pro.OrganizationName
) as C

select *
into #tmpBilledStatus 
from (
	select tdm.ClientMRN
		,tdm.ReportDOS
		,tdm.DateCorrectlyFormatted
		,tdm.ParsedClientDOS
		,tdm.ClientTransfer
		,tdm.ClientEDDisposition
		,tdm.MRNMatch
		,tdm.DaysOff
		,tdm.DOSClose
		,tdm.PatientNumber
		,tdm.ClientAccountNumber
		,case
			when s1.PatientNumber is not null then 'Y'
			else 'N'
			end as Billed
		,case
			when s2.PatientNumber is not null then 'Y'
			else 'N'
			end as PseudocodeAttached
		,tdm.OrganizationName
	from #tmpDOSMatch tdm
	left join service s1
		on tdm.PatientNumber = s1.PatientNumber
		and s1.TransToSvcNum = ''
		and s1.Amount * s1.Units > 0.01
	left join service s2
		on tdm.PatientNumber = s2.PatientNumber
		and s2.TransToSvcNum = ''
		and s2.Amount * s2.Units = 0
	group by tdm.ClientMRN
		,tdm.ReportDOS
		,tdm.DateCorrectlyFormatted
		,tdm.ParsedClientDOS
		,tdm.ClientTransfer
		,tdm.ClientEDDisposition
		,tdm.MRNMatch
		,tdm.DaysOff
		,tdm.DOSClose
		,tdm.PatientNumber
		,tdm.ClientAccountNumber
		,s1.PatientNumber
		,s2.PatientNumber
		,tdm.OrganizationName
) as D

select *
into #tmpBilledAndPseudos
from (
	select tbs.ClientMRN
		,tbs.ReportDOS
		,tbs.DateCorrectlyFormatted
		,tbs.ParsedClientDOS
		,tbs.ClientTransfer
		,tbs.ClientEDDisposition
		,tbs.MRNMatch
		,tbs.DaysOff
		,tbs.DOSClose
		,tbs.PatientNumber
		,tbs.ClientAccountNumber
		,tbs.Billed
		,tbs.PseudocodeAttached
		,tbs.OrganizationName
		,s.ServiceItemCode as Pseudocode
	from #tmpBilledStatus tbs
	left join service s
		on tbs.PatientNumber = s.PatientNumber
		and tbs.Billed = 'N'
		and tbs.PseudocodeAttached = 'Y'
	group by tbs.ClientMRN
		,tbs.ReportDOS
		,tbs.DateCorrectlyFormatted
		,tbs.ParsedClientDOS
		,tbs.ClientTransfer
		,tbs.ClientEDDisposition
		,tbs.MRNMatch
		,tbs.DaysOff
		,tbs.DOSClose
		,tbs.PatientNumber
		,tbs.ClientAccountNumber
		,tbs.Billed
		,tbs.PseudocodeAttached
		,tbs.OrganizationName
		,s.ServiceItemCode
) as E

select *
from #tmpMRNMatch tmm
left join #tmpBilledAndPseudos tbap
	on tmm.ClientMRN = tbap.ClientMRN
	and tmm.ReportDOS = tbap.ReportDOS
where isnull(tmm.ClientTransfer,'None') <> 'ED>UC'

drop table #tmpClientReport
drop table #tmpMRNMatch
drop table #tmpDOSMatch
drop table #tmpBilledStatus
drop table #tmpBilledAndPseudos



