select pyr.Name as PayorName
	,pyr.Number as PayorNumber
	,left(pp.policynumber, 3) as Prefix
	,pp.PolicyNumber
	,cln.Name as BillingClinician
	,cln.HIPAANumber as NPI
	,sum(s.Amount * s.Units) as BilledAmount
	,sum(pmt.PayAmount + pmt.AdjAmount) as Payments
	,s.PatientNumber
	,cast(s.FromDate as date) as DOS
from service s
join Payor pyr
	on s.PayorNumber = pyr.Number
	and pyr.TypeCode = 'G'
join PatientPayor pp
	on s.PatientNumber = pp.PatientNumber
	and s.PayorNumber = pp.PayorNumber
join Clinician cln
	on s.PerformedByID = cln.Number
left join IHSI_MGTPayments pmt
	on s.Number = pmt.svcnumber
	and (pmt.pmtpayor in (select number from payor where TypeCode = 'G')
		or pmt.svcpayor in (select number from payor where TypeCode = 'G'))
where s.FromDate >= '2018-11-30'
	and s.TransToSvcNum = ''
	and s.amount * s.Units > 0.01
group by s.PatientNumber
	,cast(s.FromDate as date)
	,pyr.Name
	,pyr.Number
	,pp.PolicyNumber
	,cln.Name
	,cln.HIPAANumber
