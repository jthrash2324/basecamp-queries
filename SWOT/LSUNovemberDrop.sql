select *
into #tmpMRNDOSMatch
from (
	select nlc.arrived
		,nlc.mrn
	from November2018LSUCensus nlc
	join patient p
		on cast(nlc.mrn as nvarchar(max)) = p.MedicalRecordNo
	join service s
		on p.Number = s.PatientNumber
		and nlc.arrived >= dateadd(day,-1,s.FromDate)
		and nlc.arrived <= dateadd(day,1,s.FromDate)
	group by nlc.mrn
		,nlc.arrived
) as A

select *
into #tmpRealHospAcct
from (
	select nlc.arrived
		,nlc.mrn
		,nlc.patient_name
		,max(nlc.hosp_acct) as HospAcct
	from November2018LSUCensus nlc
	left join #tmpMRNDOSMatch tsm
		on nlc.mrn = tsm.mrn
		and nlc.arrived = tsm.arrived
	where tsm.mrn is null
		--and p.AliasName is not null
	group by nlc.arrived
		,nlc.mrn
		,nlc.patient_name
) as B

select *
from #tmpRealHospAcct trha
left join Patient p
	on trha.HospAcct = cast(cast(p.AliasName as bigint) as float)
	and ISNUMERIC(p.AliasName) = 1
	and left(p.AliasName,1) <> 0
	and CHARINDEX('.',p.aliasname,1) = 0

where p.AliasName is null
order by trha.Arrived asc
--where trha.HospAcct = '9000000116676'

	--select nlc.arrived
	--	,nlc.mrn
	--	,nlc.patient_name
	--	,max(nlc.hosp_acct) as HospAcct
	--from November2018LSUCensus nlc
	--	--and p.AliasName is not null
	--left join Patient p
	--	on nlc.Hosp_Acct = cast(cast(p.AliasName as bigint) as float)
	--	and ISNUMERIC(p.AliasName) = 1
	--	and left(p.AliasName,1) <> 0
	--	and CHARINDEX('.',p.aliasname,1) = 0
	--group by nlc.arrived
	--	,nlc.mrn
	--	,nlc.patient_name
--select *
--from patient 
--where AliasName = '9000000116676'

drop table #tmpMRNDOSMatch
drop table #tmpRealHospAcct