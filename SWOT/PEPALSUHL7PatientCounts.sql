select *
into #tmpHL7
from (	
	select d.AliasName
		,cast(d.ServiceDate as date) as DOS
		,d.ProviderNumber
	from IHSI_DemographicImportTemplateUpdatable d
	where d.ServiceDate >= '2018-04-01'
	group by cast(d.ServiceDate as date)
		,d.AliasName
		,d.ProviderNumber
) as x

select *
into #tmpSums
from (
	select th.DOS
		,sum(case when th.ProviderNumber = '0000008' then 1 else 0 end) as LSUPtCount
		,sum(case when th.ProviderNumber = '0000006' then 1 else 0 end) as LIVPtCount
		,sum(case when th.ProviderNumber = '0000001' then 1 else 0 end) as MainPTCount
		,count(th.ProviderNumber) as TotalCount
	from #tmpHL7 th
	group by th.DOS
	--order by th.DOS desc
) as y



select *
into #tmpAverages
from (
	select sum(ta.TotalCount)/count(ta.DOS) as AvgTotal
		,sum(ta.LSUPtCount)/count(ta.DOS) as AvgLSU
		,sum(ta.LIVPtCount)/count(ta.DOS) as AvgLiv
		,sum(ta.MainPTCount)/count(ta.DOS) as AvgMain
	from #tmpSums ta
) as z



select ts.DOS
	,ts.LSUPtCount
	,ta.AvgLSU
	,ts.LIVPtCount
	,ta.AvgLiv
	,ts.MainPTCount
	,ta.AvgMain
	,ts.TotalCount
	,ta.AvgTotal
from #tmpSums ts
join #tmpAverages ta
	on ts.LSUPtCount < ta.AvgTotal
group by ts.DOS
	,ts.LSUPtCount
	,ta.AvgLSU
	,ts.LIVPtCount
	,ta.AvgLiv
	,ts.MainPTCount
	,ta.AvgMain
	,ts.TotalCount
	,ta.AvgTotal
order by ts.DOS desc

--select *
--from #tmpAverages ta

drop table #tmpHL7
drop table #tmpSums
drop table #tmpAverages