if (select count(s.Number) from service s where s.FromDate >= dateadd(month, -1, getdate()) and s.amount > 0) > 0 
	and ((select count(d.MessageControlID) from IHSI_DemographicImportTemplateUpdatable d where d.InsertDate >= dateadd(day, -50, getdate())) > 0
		or (select count(ei.TraceNumber) from IHSI_Eligibil_Import ei) > 0)
begin

select *
into #tmpStep1
from
(
	select distinct st.PatientNumber
		,bbp.Balance
		,st.VerifiedDate
		,s.PayorNumber
		,cast(s.FromDate as date) as DOS
		,case when DATEDIFF(day, cast(s.FromDate as date), getdate()) > 365 then 'Y' else 'N' end as YearOldAccount
		,case
			when (ei.TraceNumber is not null and d.AliasName is not null) then 'BOTH'
			when ei.TraceNumber is not null then 'ELIGIBILL ONLY'
			else 'HL7/DEMO ONLY'
			end as UpdateType
		,case
			when (d.Ins1ID is not null and ISNULL(d.Ins2ID, '') <> '') then 'BOTH'
			when d.Ins1ID is not null then 'PRIMARY'
			when ISNULL(d.Ins2ID, '') <> '' then 'SECONDARY'
			else 'NEITHER'
			end as HL7InsType
		,ei.*
		,ISNULL(d.HL7MessageTime, d.InsertDate) as HL7MessageTime
		,d.Ins1ID
		,d.Ins1PayerName
		,d.Ins1CompanyID
		,d.Ins1Name
		,d.Ins1Address1
		,d.Ins1PolicyNumber
		,d.Ins1GroupNumber
		,d.Ins1SubscriberName
		,d.Ins1SubscriberRelation
		,d.Ins2ID
		,d.Ins2PayerName
		,d.Ins2CompanyID
		,d.Ins2Name
		,d.Ins2Address1
		,d.Ins2PolicyNumber
		,d.Ins2GroupNumber
		,d.Ins2SubscriberName
		,d.Ins2SubscriberRelation
	from ihsi_staging st
	join service s
		on st.PatientNumber = s.PatientNumber
		and s.Amount * s.Units > 0.01
		and s.TransToSvcNum = ''	--Filter out transferred services
	left join Adjustment a	--Filter out invalid services
		on s.Number = a.ServiceNumber
		and a.AdjustmentTypeCode = ('019')
	join payor pyr
		on s.PayorNumber = pyr.Number
		and pyr.TypeCode = 'A'	--Billed as self-pay
	join patient p
		on s.PatientNumber = p.Number
	left join IHSI_DemographicImportTemplateUpdatable d
		on p.AliasName = d.AliasName
		and isnull(d.InsertDate, d.HL7MessageTime) >= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0)	--DEMO info arrived in the previous		month
		and isnull(d.InsertDate, d.HL7MessageTime) < DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1)
		and st.VerifiedDate <= isnull(d.InsertDate, d.HL7MessageTime)	--HL7/DEMO info arrived after verification
		and (d.Ins1ID is not null or ISNULL(d.Ins2ID, '') <> '')	--Check if the HL7 has any insurance information
		and (d.Ins1ID <> 'SP' or d.Ins1PolicyNumber <> '')	--Make sure the insurance information isn't just self pay with a blank policy number
	left join IHSI_Eligibil_Import ei	--Eligibill arrived after verification
		on s.PatientNumber = right(ei.TraceNumber, 9)
		and st.VerifiedDate <= ei.CreatedDateTime
		and LEFT(ei.CoverageType, 8) <> 'Inactive'	--Eligibill returned active coverage
	left join IHSI_BalanceByPatient bbp
		on p.Number = bbp.PatientNumber
	where st.Verified = 'Y'
		--and st.VerifiedDate < DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-2, -1)	--Verified prior to the previous month
		and a.ServiceNumber is null
		and (d.AliasName is not null or ei.TraceNumber is not null)	--Has new demo info
	--order by st.PatientNumber asc
) TS1

select *	--From here, identify whether there are mutliple eligibill/hl7 response times, only show rows where this is relevant
from #tmpStep1 ts1	
order by ts1.PatientNumber asc
	,HL7MessageTime desc

drop table #tmpStep1

end

---Review pt 8060 on PEMM LCMH