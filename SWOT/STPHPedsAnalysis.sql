
select *
into #pedsProviders
from (
	select s.PerformedByID
	,count(s.PatientNumber) as PtsSeen
	,c.Name
	,c.HIPAANumber
	,c.LastUpdatedDate as ClinicanUpdatedDate
	from service s
	join Clinician c
		on s.PerformedByID = c.Number
	where s.ProviderNumber = '0000002'
	group by s.PerformedByID
		,c.Name
		,c.HIPAANumber
		,c.LastUpdatedDate
) as x

select s.PatientNumber, p.Birthdate, s.PerformedByID, pp.PtsSeen
from service s
join patient p
	on s.PatientNumber = p.Number
	and DATEDIFF(year, p.Birthdate, s.FromDate) < 18
join #pedsProviders pp
	on s.PerformedByID = pp.PerformedByID
where s.ProviderNumber = '0000001'
group by s.PatientNumber
	,p.Birthdate
	,s.PerformedByID
	,s.FromDate
	,pp.PtsSeen
order by pp.PtsSeen desc, s.FromDate desc, p.Birthdate asc

select * from #pedsProviders

drop table #pedsProviders