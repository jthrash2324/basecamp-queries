----These 5 disctinct should be availabe for selection in the "Remittance Template" selection of RDL.

--select RemitTemplateID as RTID
--	,count(distinct number) as Pyrs
--from Payor 
--where RemitTemplateID in ('00001'
--	,'00002'
--	,'00005'
--	,'00006'
--	,'00008')
--group by RemitTemplateID
--order by RemitTemplateID asc


	
declare @remitTemplateID char(5)
declare @startPostDate date
declare @endPostDate date

set @remitTemplateID = '00001'
set @startPostDate = '2019-01-01'
set @endPostDate = '2019-02-28'

select distinct pyr.Name as PayerName
	,pay.pmtpayor as PayerNumber
	,s.PatientNumber
	,pay.PayAmount + pay.AdjAmount as PaymentAmount
	,pay.EOBReasonText as CARCCode
	,pyr.RemitTemplateID
	,'' as ServiceAdjustmentAmount
	,bbs.Balance
	,pay.PmtEntDate
	,pay.PmtNumber
	,s.Number
from IHSI_MGTPayments pay
join Payor pyr
	on pay.pmtpayor = pyr.Number
	and pyr.RemitTemplateID = @remitTemplateID
	and pay.PmtEntDate > @startPostDate
	and pay.PmtEntDate <= DATEADD(DAY, 1, @endPostDate)
join service s
	on pay.svcnumber = s.Number
	and s.ServiceStatusCode in (7, 27)
join IHSI_BalanceByService bbs
	on pay.svcnumber = bbs.ServiceNumber

--The above should yield Payer Name, Payer Number, Patient Account Number, Payment Amount (pay.PayAmt + pay.AdjAmount), CARC Code (EOB Reason Test), and Balance. Join to the adjustment table on service number as well as pmtentdate = adjustment.entereddate and adjustment.adjtype = 'S'. Let me know if there are any many to 1 adjustments to payments based on this scenario and I will review.

