select 
	distinct s.PatientNumber
	,s.Number as ServiceNumber
	,cast(s.FromDate as date) as DOS
	,s.Amount
	,bbs.Balance as ServiceBalance
	,bbp.Balance as PatientBalance
	,s.ServiceItemCode
	,s.ServiceStatusCode
	,pay.EOBReasonText
from service s
join Payment pay
	on s.Number = pay.ServiceNumber
	and pay.EOBReasonText like 'CO%96%'
join IHSI_BalanceByService bbs
	on s.Number = bbs.ServiceNumber
join IHSI_BalanceByPatient bbp
	on s.PatientNumber = bbp.PatientNumber
where s.ServiceStatusCode not in (40, 50)
	and s.ServiceItemCode in ('99053', '93010', '99406', '94760')
