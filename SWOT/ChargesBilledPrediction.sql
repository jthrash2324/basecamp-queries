select *
into #tmpPostedPeriod
from (
	select s.PostedPeriod
		--,s.ProviderNumber
		,min(cast(s.entereddate as date)) as PeriodStart
		,max(cast(s.entereddate as date)) as PeriodEnd
		,cast(DATEADD(month, DATEDIFF(month, -3, min(cast(s.entereddate as date))) - 4, 0) as date) as FirtDayPrevious2Months
	    ,cast(DATEADD(ss, -1, DATEADD(month, DATEDIFF(month, -2, min(cast(s.entereddate as date))) - 2, 0)) as date) as		LastDayPrevious2Months
		--,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,max(cast(s.entereddate as date))),0))
		,right(s.postedperiod, 2) as PostedPeriodMonth
		,datediff(day, min(cast(s.entereddate as date)), max(cast(s.entereddate as date))) as DaysInPeriod
		,datediff(day, -7, max(cast(s.entereddate as date)))/7-datediff(day, -6, min(cast(s.entereddate as date)))/7 AS MON
		,datediff(day, -6, max(cast(s.entereddate as date)))/7-datediff(day, -5, min(cast(s.entereddate as date)))/7 AS TUE
		,datediff(day, -5, max(cast(s.entereddate as date)))/7-datediff(day, -4, min(cast(s.entereddate as date)))/7 AS WED
		,datediff(day, -4, max(cast(s.entereddate as date)))/7-datediff(day, -3, min(cast(s.entereddate as date)))/7 AS THU
		,datediff(day, -3, max(cast(s.entereddate as date)))/7-datediff(day, -2, min(cast(s.entereddate as date)))/7 AS FRI
		,datediff(day, -2, max(cast(s.entereddate as date)))/7-datediff(day, -1, min(cast(s.entereddate as date)))/7 AS SAT
		,datediff(day, -1, max(cast(s.entereddate as date)))/7-datediff(day, 0, min(cast(s.entereddate as date)))/7 AS SUN
		,count(distinct s.PatientNumber) as BilledPatients
		,count(distinct s.Number) as BilledSrvs
		,sum(s.Amount * s.Units) as ChargesBilled
	from Service s
	where s.TransFromSvcNum = ''
		and s.Amount > 0.01
	group by s.PostedPeriod
		--,s.ProviderNumber
) as PP

select *
into #tmpDOS
from (
	select tpp.PostedPeriod
		,tpp.FirtDayPrevious2Months
		,tpp.LastDayPrevious2Months
		,count(distinct s.PatientNumber) as PtsPrevious2MOS
		,datediff(day, tpp.FirtDayPrevious2Months, tpp.LastDayPrevious2Months) as DOSPrevious2MOS
		,round(sum(s.Amount * s.Units), 2) as ChargesPrevious2MOS
	from #tmpPostedPeriod tpp
	left join service s
		on cast(s.FromDate as date) >= tpp.FirtDayPrevious2Months
			and cast(s.FromDate as date) <= tpp.LastDayPrevious2Months
			and s.Amount * s.Units > 0
			and s.TransToSvcNum = ''
	group by tpp.PostedPeriod
		,tpp.FirtDayPrevious2Months
		,tpp.LastDayPrevious2Months
	--order by tpp.PostedPeriod desc
) as DOS

select *
from #tmpPostedPeriod tpp
left join #tmpDOS tdos
	on tpp.PostedPeriod = tdos.PostedPeriod

drop table #tmpDOS
drop table #tmpPostedPeriod