select 
	pay.Name as PayorName
	,pay.Number as PayorNumber
	,pp.PolicyNumber
	,cl.Name as BillingClinician
	,cl.HIPAANumber as NPI
	,cast(c.BilledAmount as nvarchar(50)) as BilledAmount
	,cast(sum(pmt.PayAmount + pmt.AdjAmount) as nvarchar(50)) as Payments
	,s.PatientNumber
	,cast(s.FromDate as date) as DOS
from IHSI_PayorType pt
join payor pay
	on pt.Code = pay.TypeCode
join IHSI_MGTPayments pmt
	on pay.Number = pmt.pmtpayor
join service s
	on pmt.svcnumber = s.Number
join CashReceipt cr
	on pmt.crnumber = cr.Number
join Claim c
	on cr.ClaimNumber = c.Number
join Clinician cl
	on s.PerformedByID = cl.Number
join PatientPayor pp
	on pay.Number = pp.PayorNumber
	and s.PatientNumber = pp.PatientNumber
where 
	pt.Code = 'G'
	and s.FromDate >= '2018-11-30'
group by 
	pay.Name
	,pay.Number
	,cl.Name
	,cl.HIPAANumber
	,c.BilledAmount
	,s.PatientNumber
	,cast(s.FromDate as date)
	,pp.PolicyNumber


union

select pay.Name as PayorName
	,pay.Number as PayorNumber
	,pp.PolicyNumber
	,cl.Name as BillingClinician
	,cl.HIPAANumber as NPI
	,isnull(cast(clm.BilledAmount as nvarchar(50)), 'PENDING') AS BilledAmount
	,'NO RESPONSE' as Payments
	,s.PatientNumber
	,cast(s.FromDate as date) as DOS
from service s
join Payor pay
	on s.PayorNumber = pay.Number
left join IHSI_MGTPayments pmt
	on s.Number = pmt.svcnumber
left join Payor paypmt
	on pmt.pmtpayor = paypmt.Number
	and paypmt.TypeCode = 'G'
join PatientPayor pp
	on s.PatientNumber = pp.PatientNumber
	and s.PayorNumber = pp.PayorNumber
join Clinician cl
	on s.PerformedByID = cl.number
left join Claim clm
	on s.ClaimNumber = clm.Number
where pay.TypeCode = 'G'
	and s.FromDate >= '2018-11-30'
	and isnull(s.TransFromSvcNum, '') = ''
	and paypmt.Number is null
	and s.Amount * s.Units > 0.01
group by pay.Name 
	,pay.Number
	,pp.PolicyNumber
	,cl.Name
	,cl.HIPAANumber 
	,clm.BilledAmount
	,s.PatientNumber
	,cast(s.FromDate as date)