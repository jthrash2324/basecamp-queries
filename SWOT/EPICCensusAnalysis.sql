select *
into #tmpStep1
from (

	select cast(cast(trl.Hosp_Acct as bigint) as char(30)) as HospitalAccountNumber
		,case
			when p.AliasName is null then 'N'
			else 'Y'
		end as AccountNumberInIHSIDB
		,case
			when s1.PatientNumber is not null then 'Y'
			else 'N'
		end as ChargesBilled
		,s1.ProviderNumber as BilledLocation
		,case
			when s2.PatientNumber is not null then 'Y'
			else 'N'
		end as PseudoCodesPresent
		,s2.ProviderNumber as UnbillLocation
		,isnull(datepart(month,s1.FromDate),datepart(month,s2.fromdate)) as ServiceMonth
	from TotalReconLSU trl
	left join patient p
		on cast(cast(trl.Hosp_Acct as bigint) as char(30)) = p.AliasName
	left join service s1
		on p.Number = s1.PatientNumber
		and s1.Amount * s1.Units > 0.01
	left join service s2
		on p.Number = s2.PatientNumber
		and s2.Amount * s2.Units = 0
	group by cast(cast(trl.Hosp_Acct as bigint) as char(30)) 
		,p.AliasName
		,s1.PatientNumber
		,s2.PatientNumber
		,s1.FromDate
		,s2.FromDate
		,s1.ProviderNumber
		,s2.ProviderNumber
) as x

select *
into #tmpStep2
from (

	select ts1.HospitalAccountNumber
		,case
			when ts1.ChargesBilled = 'N' and ts1.PseudoCodesPresent = 'N' then 'NA'
			when ts1.ChargesBilled = 'Y' then 'Billed'
			when ts1.ChargesBilled = 'N' and ts1.PseudoCodesPresent = 'Y' then 'Unbill'
		end as ReportingType
		,s.ServiceItemCode
		,s.FromDate as DOS
		,p.Birthdate
		,case
			when DATEDIFF(year, p.Birthdate, s.FromDate) < 18 then 'Y'
			else 'N'
		end as Minor
		,ts1.ServiceMonth
		,isnull(ts1.BilledLocation, ts1.UnbillLocation) as ProviderNumber
	from #tmpStep1 ts1
	left join patient p
		on ts1.HospitalAccountNumber = p.AliasName
	left join service s
		on p.Number = s.PatientNumber
		and ts1.ChargesBilled = 'N'
		and ts1.PseudoCodesPresent = 'Y'
	group by ts1.HospitalAccountNumber
		,ts1.ChargesBilled
		,ts1.PseudoCodesPresent
		,s.ServiceItemCode
		,ts1.ServiceMonth
		,ts1.BilledLocation
		,ts1.unbilllocation
		,p.Birthdate
		,s.FromDate
) as y

select ts2.ServiceMonth
	,sum(case when ts2.ReportingType = 'Billed' then 1 else 0 end) as TotalBilled
	,sum(case when ts2.ReportingType = 'Billed' and ts2.ProviderNumber = '0000008' then 1 else 0 end) as BilledPtCountLSU
	,sum(case when ts2.ReportingType = 'Billed' and ts2.ProviderNumber <> '0000008' then 1 else 0 end) as BilledPtCountOtherLocation
	,sum(case when ts2.ReportingType = 'Unbill' and ts2.ProviderNumber = '0000008' then 1 else 0 end) as TotalUnbillCountLSU
	,sum(case when ts2.ReportingType = 'Unbill' and ts2.ProviderNumber <> '0000008' then 1 else 0 end) as TotalUnbillCountOtherLocation
	,sum(case when ts2.ReportingType = 'Unbill' and ts2.ServiceItemCode = 'TRANS' then 1 else 0 end) as TRANS
	,sum(case when ts2.ReportingType = 'Unbill' and ts2.ServiceItemCode = 'lwobs' then 1 else 0 end) as LWOBS
	,sum(case when ts2.ReportingType = 'Unbill' and ts2.ServiceItemCode = 'nsbep' and ts2.Minor = 'Y' then 1 else 0 end) as NSBEPMinor
	,sum(case when ts2.ReportingType = 'Unbill' and ts2.ServiceItemCode = 'nsbep' and ts2.Minor = 'N' then 1 else 0 end) as NSBEPAdult
	,sum(case when ts2.ReportingType = 'Unbill' and ts2.ServiceItemCode = 'docum' then 1 else 0 end) as DOCUM
from #tmpStep2 ts2
group by ts2.ServiceMonth
order by ts2.ServiceMonth asc

--select ts2.ServiceItemCode
--	,count(ts2.hospitalaccountnumber) as PtCount
--from #tmpStep2 ts2
--group by ts2.ServiceItemCode

select count(distinct s.PatientNumber)
from service s
join patient p
	on s.PatientNumber = p.number
left join TotalReconLSU trl
	on cast(cast(trl.Hosp_Acct as bigint) as char(30)) = p.AliasName
where s.ProviderNumber = '0000008'
	and s.Amount * s.Units > 0.01
	and trl.Hosp_Acct is null
	and s.FromDate >= '2018-04-01'
	and s.FromDate < '2018-10-01'

select * from #tmpStep2

drop table #tmpStep1
drop table #tmpStep2