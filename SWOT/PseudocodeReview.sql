/*Used so the account management team can review which pseudocodes are used on their client and how they should show up on the Monthly Summary reports. This is a pre-step to developing the Executive Summary

Written 12/20/2018 by Justin Thrash

*/

select pc.Active
	,pc.Code
	,pc.Description
	,pc.StandAloneCode
	,count(distinct s.patientnumber) as Uses2018
	,count(distinct s2.PatientNumber) as PatientsAlsoBilled
from IHSI_PseudoCodes pc
left join service s
	on pc.Code = s.ServiceItemCode
	and s.TransToSvcNum = ''
	and s.EnteredDate >= '2018-01-01'
left join service s2
	on s.PatientNumber = s2.PatientNumber
	and s2.TransToSvcNum = ''
	and s2.EnteredDate >= '2018-01-01'
	and s2.Amount * s2.Units > 0.01
left join adjustment a
	on s.Number = a.ServiceNumber
	and a.AdjustmentTypeCode in ('015','019')
left join Adjustment a2
	on s2.Number = a2.ServiceNumber
	and a2.AdjustmentTypeCode in ('015','019')
where pc.Active = 'Y'
	and a.ServiceNumber is null
	and a2.ServiceNumber is null
group by pc.Active
	,pc.Code
	,pc.Description
	,pc.StandAloneCode
--	,s2.PatientNumber
having count(distinct s.patientnumber) > 1
order by Uses2018 desc