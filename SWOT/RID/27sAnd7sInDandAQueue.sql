declare @userid char(4) = 'T018'

select rq.*
	,ra.*
	,*
from service s
left join service s2
	on s.PatientNumber = s2.PatientNumber
	and s.ServiceItemCode = s2.ServiceItemCode
	and s2.ClaimNumber <> ''
left join IHSI_ReimbQueue rq
	on s2.ClaimNumber = rq.ClaimNumber

	left join IHSI_ReimbActivity RA on RA.QueueID=RQ.QueueID 
			AND (ra.ActivityID IN  --Added 2010/10/25 --moved back to a join on 6/30/11 by CW: Retrieves the last activity.
				(SELECT MAX(RA1.ActivityID) as ActivityID 
					FROM dbo.IHSI_ReimbQueue as RQ with(nolock)
						  INNER JOIN dbo.IHSI_ReimbActivity as RA1 with(nolock) on RA1.queueID = RQ.queueID
						  LEFT JOIN dbo.IHSI_ReimbActivityReasonAssignment as RARA with(nolock) on RARA.ActivityID = RA1.ActivityID
					WHERE rara.ActivityID IS NOT NULL 				
					--OR (rara.ActivityID IS NULL  ---2012-04-26: Removed to allow the last Worked Date to pick up items with an Action vs. ones that are only viewed.
					--	AND RA1.queueID NOT IN (
					--		SELECT RA2.queueID 
					--		FROM dbo.IHSI_ReimbActivity as RA2 with(nolock)
					--			INNER JOIN dbo.IHSI_ReimbActivityReasonAssignment as RARA2 with(nolock) on RARA2.ActivityID = RA2.ActivityID
					--		GROUP BY RA2.queueID))
						GROUP BY RQ.QueueID
					) 
			OR ra.ActivityID IS NULL)

where s.ServiceStatusCode in (7, 27)
	and s.ClaimNumber = ''
	and s2.PatientNumber is not null


	and (
				rq.Active = 'Y' 
				OR rq.Denial = 'Y' 
				OR rq.Aging = 'Y'
				OR (ra.ActivityEndTime >= Convert(Datetime, Convert(char(10), GetDate(), 101)) AND ra.WorkedByID = @UserID)
				)		
			AND rq.QueueLock = 'N'
			AND (rq.AssignedToID = @UserID OR rq.AssignedToID IS NULL OR rq.AssignedToID = '')

--select top 100 * from IHSI_ReimbQueue