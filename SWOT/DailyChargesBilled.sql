select s.PostedPeriod
	,cast(s.EnteredDate as date) as EnteredDate
	,count(distinct s.PatientNumber) as PtsBilled
	,count(distinct s.Number) as ServicesBilled
	,sum(s.amount * s.Units) as ChargesBilled
	,Datename(weekday, DATEPART(WEEKDAY, cast(s.EnteredDate as date))) as DayOfWeek
from service s
where s.Amount > 0.01
	and s.TransToSvcNum = ''
group by cast(s.EnteredDate as date)
	,s.PostedPeriod
order by EnteredDate desc