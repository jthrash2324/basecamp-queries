select *
into #tmpPostVerification
from
(

	select c.PatientNumber
		,p.AliasName
		,p.MedicalRecordNo
		,isnull(cr.PayorNumber, c.Number) as PayorNumber	--Pull payor number off cash receipt if it exists
		,c.Number as ClaimNumber
		,c.PrintedDate
		,c.LastReprintDate
		,case
			when sum(pay.PayAmount + pay.AdjAmount) > 0 then 'Paid'
			when sum(pay.PayAmount + pay.AdjAmount) = 0 then 'Denied'
			else 'Rejected/No Response?'
			end as 'Status'
		,cast(sum(pay.PayAmount + pay.AdjAmount) as decimal(18,2)) as TotalPayments
		,pyr.Name as PayorName	--If Cash Receipt payor number is different than the Claim payor, this will pull the info	associated with the CR payor
		,pyr.AddressLine1
		,pyr.AddressLine2
		,pyr.City
		,pyr.State
		,pyr.ZipCode
		,isnull(ppg.SubscriberName, pp.SubscriberName) as GuarantorName
		,isnull(ppg.SubscriberBirthdate, pp.SubscriberBirthdate) as GuarantorBirthdate
		,isnull(ppg.SubscriberSocSecNo, pp.SubscriberSocSecNo) as GuarantorSocSecNo
		,pp.SubscriberName
		,pp.SubscriberBirthdate
		,pp.SubscriberSocSecNo
		,pp.GroupNumber
		,pp.PolicyNumber
		,pp.SubscriberAddress1
		,pp.SubscriberAddress2
		,pp.SubscriberCity
		,pp.SubscriberState
		,pp.SubscriberZipCode
	from claim c
	left join CashReceipt cr
		on c.Number = cr.ClaimNumber
	left join IHSI_MGTPayments pay
		on cr.Number = pay.crnumber
	left join payor pyr
		on isnull(cr.PayorNumber, c.PayorNumber) = pyr.Number
		and pyr.Name is not null	--Are Null records are from deleted payors?
	left join PatientPayor pp
		on c.PatientNumber = pp.PatientNumber
		and c.PayorNumber = pp.PayorNumber
		and pp.Type = 'P'
	left join patientpayor ppg	--Grab Guarantor info if it exists
		on c.PatientNumber = ppg.PatientNumber
		and ppg.Type = 'G'
	left join patient p
		on c.PatientNumber = p.Number
	where c.LastReprintDate <= DATEADD(day,-45,getdate())	--Give the insurance a chance to pay
		and c.LastReprintDate >= dateadd(month, -2, getdate())	--Narrow results to last 2 month for simplicity's sake
		and pp.Type = 'P'	--To start let's look at primary payors only
		--and pyr.TypeCode <> 'A'
	group by c.PatientNumber
		,p.AliasName
		,p.MedicalRecordNo
		,cr.PayorNumber
		,c.Number
		,c.PrintedDate
		,c.LastReprintDate
		,pyr.Name
		,pyr.AddressLine1
		,pyr.AddressLine2
		,pyr.City
		,pyr.State
		,pyr.ZipCode
		,ppg.SubscriberName
		,ppg.SubscriberBirthdate
		,ppg.SubscriberSocSecNo
		,pp.SubscriberName
		,pp.SubscriberBirthdate
		,pp.SubscriberSocSecNo
		,pp.GroupNumber
		,pp.PolicyNumber
		,pp.SubscriberAddress1
		,pp.SubscriberAddress2
		,pp.SubscriberCity
		,pp.SubscriberState
		,pp.SubscriberZipCode
) as POV

select *
into #tmpPreVerification
from
(

	select d.AliasName
		,d.MedicalRecNo
		,d.Ins1ID
		,d.Ins1CompanyID
		,d.Ins1Name
		,d.Ins1Address1
		,d.Ins1Address2
		,d.Ins1City
		,d.Ins1State
		,d.Ins1Zip
		,d.GuarantorName
		,d.GuarantorBirthdate
		,d.GuarantorSSN
		,d.Ins1SubscriberName
		,d.Ins1SubscriberDOB
		,d.Ins1SubscriberSSN
		,d.Ins1GroupNumber
		,d.Ins1PolicyNumber
		,d.Ins1SubscriberAddress1
		,d.Ins1SubscriberAddress2
		,d.Ins1SubscriberCity
		,d.Ins1SubscriberState
		,d.Ins1SubscriberZip
		--,*
	from #tmpPostVerification tpv
	left join IHSI_DemographicImportTemplateUpdatable d
		on tpv.aliasname = d.AliasName
		and d.Imported = 1
	left join IHSI_DemographicImportTemplateUpdatable d2
		on tpv.AliasName = d2.AliasName
		and ISNULL(d2.HL7MessageTime, d2.InsertDate) > isnull(d.HL7MessageTime, d.InsertDate)	--Just pulling the top HL7 message for now, will add others (both imported and non) later
		and d2.Imported = 1
	where d2.AliasName is null
) as PIV

select *
into #tmpUnion
from (

	select 'HL7' as RowType
		,'' as PatientNumber
		,pv.AliasName
		,pv.MedicalRecNo as MedicalRecordNo
		,pv.Ins1ID + '-' + pv.Ins1CompanyID as PayorNumber
		,'' as ClaimNumber
		,'' as PrintedDate
		,'' as LastReprintDate
		,'' as Status
		,'999999999' as TotalPayments
		,pv.Ins1Name as PayorName
		,pv.Ins1Address1 as AddressLine1
		,pv.Ins1Address2 as AddressLine2
		,pv.Ins1City as City
		,pv.Ins1State as State
		,pv.Ins1Zip as ZipCode
		,pv.GuarantorName
		,pv.GuarantorBirthdate
		,pv.GuarantorSSN as GuarantorSocSecNo
		,pv.Ins1SubscriberName as SubscriberName
		,pv.Ins1SubscriberDOB as SubscriberBirthdate
		,pv.Ins1SubscriberSSN as SubscriberSocSecNo
		,pv.Ins1GroupNumber as GroupNumber
		,pv.Ins1PolicyNumber as PolicyNumber
		,pv.Ins1SubscriberAddress1 as SubscriberAddress1
		,pv.Ins1SubscriberAddress2 as SubscriberAddress2
		,pv.Ins1SubscriberCity as SubscriberCity
		,pv.Ins1SubscriberState as SubscriberState
		,pv.Ins1SubscriberZip as SubscriberZipCode
	from #tmpPreVerification pv
	
	union
	
	select 'Verification' as RowType
		,tpv.PatientNumber
		,tpv.AliasName
		,tpv.MedicalRecordNo
		,tpv.PayorNumber
		,tpv.ClaimNumber
		,tpv.PrintedDate
		,tpv.LastReprintDate
		,tpv.Status
		,tpv.TotalPayments
		,tpv.PayorName
		,tpv.AddressLine1
		,tpv.AddressLine2
		,tpv.City
		,tpv.State
		,tpv.ZipCode
		,tpv.GuarantorName
		,tpv.GuarantorBirthdate
		,tpv.GuarantorSocSecNo
		,tpv.SubscriberName
		,tpv.SubscriberBirthdate
		,tpv.SubscriberSocSecNo
		,tpv.GroupNumber
		,tpv.PolicyNumber
		,tpv.SubscriberAddress1
		,tpv.SubscriberAddress2
		,tpv.SubscriberCity
		,tpv.SubscriberState
		,tpv.SubscriberZipCode
	from #tmpPostVerification tpv
) as U

select *
into #tmpComparable
from 
(

select tu.RowType
	,tu.PatientNumber
	,cast(tu.AliasName as varchar(max)) as AliasName
	,tu.MedicalRecordNo
	,tu.PayorNumber
	,tu.ClaimNumber
 	,tu.PrintedDate
	,tu.LastReprintDate
	,tu.Status
	,tu.TotalPayments
	,tu.PayorName
	,tu.AddressLine1
	,tu.AddressLine2
	,tu.City
	,tu.State
	,tu.ZipCode
	,tu.GuarantorName
	,tu.GuarantorBirthdate
	,tu.GuarantorSocSecNo
	,tu.SubscriberName
	,tu.SubscriberBirthdate
	,tu.SubscriberSocSecNo
	,cast(tu.GroupNumber as varchar(max)) as GroupNumber
	,cast(tu.PolicyNumber as varchar(max)) as PolicyNumber
	,tu.SubscriberAddress1
	,tu.SubscriberAddress2
	,tu.SubscriberCity
	,tu.SubscriberState
	,tu.SubscriberZipCode
from #tmpUnion tu
join #tmpUnion tu2
	on tu.aliasname = tu2.aliasname
	and tu.rowtype <> tu2.rowtype
	and tu2.addressline1 is not null
where tu.addressline1 is not null

) as TC

select distinct *
from #tmpComparable tc
order by tc.AliasName asc
	,tc.RowType asc


drop table #tmpPostVerification
drop table #tmpPreVerification
drop table #tmpUnion
drop table #tmpComparable