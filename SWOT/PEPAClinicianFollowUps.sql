select *
into #tmpClinicianData
from
(
	select c.Name as Clinician
		,c.Number
		,count(distinct s.PatientNumber) as TotalPatientsBilling
		,'' as TotalPatientsServicing
		,'' as FollowUpBilling
		,'' FollowUpServicing
	from Clinician c
	join Service s
		on c.Number = s.PerformedByID	--Billing
		and s.FromDate >= '2018-10-01'
		and s.FromDate < '2019-01-01'
	group by c.Name
		,c.Number

	union

	select c.Name as Clinician
		,c.Number
		,'' as TotalPatientsBilling
		,count(distinct s.PatientNumber) as TotalPatientsServicing
		,'' as FollowUpBilling
		,'' FollowUpServicing
	from Clinician c
	join Admission a
		on c.Number = a.AttendingClinicianID
	join Service s
		on a.Number = s.AdmissionNumber
		and s.FromDate >= '2018-10-01'
		and s.FromDate < '2019-01-01'
	group by c.Name
		,c.Number

	union

	select c.Name as Clinician
		,c.Number
		,'' as TotalPatientsBilling
		,'' as TotalPatientsServicing
		,count(distinct ce.PatientNumber) as FollowUpBilling
		,'' FollowUpServicing
	from Clinician c
	join Audit_IHSI_CodingEntries ce
		on c.Number = ce.BillingClinician
		and ce.DateOfService >= '2018-10-01'
		and ce.DateOfService < '2019-01-01'
		and ce.ServiceItemCode = 'DOCUM'
	group by c.Name
		,c.Number

	union

	select c.Name as Clinician
		,c.Number
		,'' as TotalPatientsBilling
		,'' as TotalPatientsServicing
		,'' as FollowUpBilling
		,count(distinct ce.PatientNumber) as FollowUpServicing
	from Clinician c
	join Audit_IHSI_CodingEntries ce
		on c.Number = ce.ServicingClinician
		and ce.DateOfService >= '2018-10-01'
		and ce.DateOfService < '2019-01-01'
		and ce.ServiceItemCode = 'DOCUM'
	group by c.Name
		,c.Number
) A

select tcd.Clinician
	,tcd.Number
	,sum(tcd.TotalPatientsBilling) as TotalPatientsBilling
	,sum(tcd.TotalPatientsServicing) as TotalPatientsServicing
	,sum(tcd.FollowUpBilling) as FollowUpBilling
	,sum(tcd.FollowUpServicing) as FollowUpServicing
	--,case
	--	when sum(tcd.TotalPatientsBilling) = 0 then 0
	--	else (sum(tcd.FollowUpBilling) / sum(tcd.TotalPatientsBilling)) * 100 
	--	end as FollowUpPercentBilling
from #tmpClinicianData tcd
group by tcd.Clinician
	,tcd.Number
order by tcd.Clinician asc

drop table #tmpClinicianData


