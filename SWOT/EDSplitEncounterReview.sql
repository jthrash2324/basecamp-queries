select st.patientnumber
	,s1.Number
	,s1.ServiceItemCode
	,s1.PerformedByID
	,s1.ClaimNumber
	,s2.Number
	,s2.ServiceItemCode
	,s2.PerformedByID
	,s2.ClaimNumber
from IHSI_Staging st
join service s1
	on st.PatientNumber = s1.PatientNumber
	and s1.amount * s1.Units > 0.01
join service s2
	on st.PatientNumber = s2.PatientNumber
	and s1.PerformedByID <> s2.PerformedByID
	and s2.Amount * s2.Units > 0.01
	and s1.ServiceItemCode < s2.ServiceItemCode
	and cast(s1.FromDate as date) = cast(s2.FromDate as date)
