select *
into #tmpBilled
from
(

	select m.Arrived
		,SUBSTRING(m.Arrived,2,2) as ArrivalMonth
		,datepart(month,s.FromDate) as BilledMonth
		,cast(cast(m.Hosp_Acct as bigint) as char(30)) as Hosp_Acct
		,p.AliasName as MatchingPatientAlias
		,p.Number as PatientNumber
		,m.MRN
		,m.Patient_Name
		,m.ED_Disposition
		,s.ProviderNumber
		--,s.ServiceItemCode
		,case
			when s.ProviderNumber = '0000008' then 'Y'
			else 'N'
		end as BilledUnderLSU
		,case
			when s.ProviderNumber <> '0000008' then 'Y'
			else 'N'
		end as BilledUnderOtherLocation
		,s2.ProviderNumber as UnbillProvider
		,s2.ServiceItemCode
		,s2.PatientNumber as UnbillPtNumber
	from TotalReconLSU m
	left join patient p
		on cast(cast(m.Hosp_Acct as bigint) as char(30)) = p.AliasName
	left join service s
		on p.Number = s.PatientNumber
		and s.Amount * s.Units > 0.01
	left join service s2
		on p.Number = s2.Number
		and s.PatientNumber <> s2.PatientNumber
		and s2.Amount * s2.Units = 0
	--left join service s2
	--	on p.Number = s2.PatientNumber
	--	and s2.Units * s2.Amount > 0.01
		--and ISNUMERIC(s.ServiceItemCode) = 1
	--where s.ProviderNumber = '0000008'
	--where s.ServiceItemCode <> 'NSBEP'
	--	and ISNUMERIC(s.ServiceItemCode) = 1
		--and s.ProviderNumber <> '0000008'
	--where s.Amount * s.units > 0.01
		--and s.ProviderNumber <> '0000008'
		--and s2.PatientNumber is null
		--and (m.ED_Disposition <> 'Discharge' or s.ProviderNumber <> '0000008')
		--and m.ED_Disposition not like '%discharge%'
	group by m.Arrived
		,cast(cast(m.Hosp_Acct as bigint) as char(30))
		,p.AliasName
		,p.Number
		,m.MRN
		,m.Patient_Name
		,m.ED_Disposition
		,s.ProviderNumber
		,s.FromDate
		,s2.ProviderNumber
		,s2.ServiceItemCode
		,s2.PatientNumber
		--,s.ServiceItemCode
) as x

select *
from #tmpBilled

select *
into #tmpLocation
from
(
	select tb.ArrivalMonth as MonthOfServiceBilled
		,count(distinct tb.Hosp_Acct) as PtCount
		,tb.BilledUnderLSU
		,tb.BilledUnderOtherLocation
	from #tmpBilled tb
	group by tb.ArrivalMonth
		,tb.BilledUnderLSU
		,tb.BilledUnderOtherLocation
	--order by tb.BilledMonth desc
) as y

--select tl.MonthOfServiceBilled
--	,tl.PtCount + tl2.PtCount as TotalPatientNumbersBilled
--	,tl.PtCount as PatientsBilledLSU
--	,tl2.PtCount as PatientsBilledOther
--from #tmpLocation tl
--left join #tmpLocation tl2
--	on tl.MonthOfServiceBilled = tl2.MonthOfServiceBilled
--where tl.BilledUnderLSU = 'Y'
--	and tl2.BilledUnderOtherLocation = 'Y'
--order by tl.MonthOfServiceBilled asc

--select trl.Arrived
--	--,SUBSTRING(trl.Arrived, 2, 2) as Month
--	--,SUBSTRING(trl.Arrived,5,2) as Day
--	,cast('2018-' + SUBSTRING(trl.Arrived, 2, 2) + '-' + SUBSTRING(trl.Arrived,5,2) as Date)  as Date
--	,trl.Hosp_Acct
--	,trl.MRN
--	,trl.Patient_Name
--	,trl.ED_Disposition	---MRN Match
--	,p.AliasName
--	,p.MedicalRecordNo
--	,p.Name
--	,p.number
--	,s.FromDate
--	,DATEDIFF(day, cast('2018-' + SUBSTRING(trl.Arrived, 2, 2) + '-' + SUBSTRING(trl.Arrived,5,2) as Date), s.FromDate)
--from TotalReconLSU trl
--left join #tmpBilled tb
--	on trl.Hosp_Acct = tb.Hosp_Acct
--join patient p
--	on cast(cast(trl.MRN as bigint) as char(30)) = p.MedicalRecordNo
--join service s
--	on p.Number = s.PatientNumber
--	and s.Amount * s.Units > 0.01
--where tb.Hosp_Acct is null
--	and cast(cast(trl.Hosp_Acct as bigint) as char(30)) <> p.AliasName
--	and DATEDIFF(day, cast('2018-' + SUBSTRING(trl.Arrived, 2, 2) + '-' + SUBSTRING(trl.Arrived,5,2) as Date), s.FromDate) < 3
--	and DATEDIFF(day, cast('2018-' + SUBSTRING(trl.Arrived, 2, 2) + '-' + SUBSTRING(trl.Arrived,5,2) as Date), s.FromDate) > -3
--group by trl.Arrived
--	,trl.Hosp_Acct
--	,trl.MRN
--	,trl.Patient_Name
--	,trl.ED_Disposition
--	,p.AliasName
--	,p.MedicalRecordNo
--	,p.Name
--	,p.number
--	,s.FromDate
--select * from #tmpBilled

--select * from #tmpLocation

drop table #tmpBilled
drop table #tmpLocation

/*
	1913 have provider 0000008
	ED Month Sum Census is 4778
	Billed Patient Count is 1158

	The Dr. K spreadsheet shows 1472 "Total Patients Actually Treated in ER" for May 2018.
	The census Amanda pulled from EPIC contains 2191 distinct patients.
	We billed 1424 of the patients within the PEPA database.
		1151 of these were billed under NBR/LUS (provider 8).
		273 were billed under other places.

	The Dr. K spreadheet does have a few repeats in it (~5).

	We have 3 more LWOBS than they do.
*/