--update IHSI_PayorTranslation
--set ins_id = 987

--update IHSI_PayorTranslation
--set PayorNumber = '0000002'
--	,Linked = 'Y'
--where ins_id = '200'


select 	count(s.number) as CodedServices
	,pay.Number
	,pt.ins_id
	,pt.Description
	,pay.Name
	,pt.Address_1
	,pay.AddressLine1
	,pt.Address_2
	,pay.AddressLine2
	,pt.City
	,pay.City
	,pt.State
	,pay.State
	,pt.Zip_Code
	,pay.ZipCode
	,pt.Phone_1
	,pay.TelephoneNumber

from IHSI_PayorTranslation pt
left join payor pay
	on pt.Address_1 = pay.AddressLine1
left join service s
	on pay.Number = s.PayorNumber
	and s.amount > 0.01
	and s.TransToSvcNum = ''
	and s.FromDate >= dateadd(month,-6,getdate())
--where ins_id = '200'
where pt.Linked = 'N'
group by pt.ins_id
	,pt.Description
	,pay.Name
	,pt.Address_1
	,pay.AddressLine1
	,pt.Address_2
	,pay.AddressLine2
	,pt.City
	,pay.City
	,pt.State
	,pay.State
	,pt.Zip_Code
	,pay.ZipCode
	,pt.Phone_1
	,pay.TelephoneNumber
	,pay.Number
order by pt.ins_id asc

--select distinct d.Ins1ID
--	,d.Ins1PayerName
--	,d.Ins1CompanyID
--	,d.Ins1Name
--	,d.Ins1Address1
--	,d.Ins1Address2
--	,d.Ins1City
--	,d.Ins1State
--	,d.Ins1Zip
--	,count(distinct d.AliasName) as Pts
--from IHSI_DemographicImportTemplateUpdatable d
--where d.Ins1Name <> ''
----and d.Ins1ID = '200'
--group by d.Ins1ID
--	,d.Ins1PayerName
--	,d.Ins1CompanyID
--	,d.Ins1Name
--	,d.Ins1Address1
--	,d.Ins1Address2
--	,d.Ins1City
--	,d.Ins1State
--	,d.Ins1Zip
--order by pts desc

--update IHSI_DemographicImportTemplateUpdatable 
--set ins2id = Ins2CompanyID
--where len(ins2id) = 1

--select ins2id, Ins2CompanyID, * from IHSI_DemographicImportTemplateUpdatable where len(ins2id) = 1