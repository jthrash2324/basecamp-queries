select *
into #tmpPrimaryPayorAnalysis
from
(

	select d.AliasName
		,d.Ins1PolicyNumber
		,d.InsertDate
		,st.VerifiedDate
		,pp.PayorNumber
	from IHSI_DemographicImportTemplateUpdatable d
	left join IHSI_DemographicImportTemplateUpdatable d2
		on d.AliasName = d2.AliasName
		and d2.Imported = 1
		and isnull(d2.HL7MessageTime, d2.InsertDate) > isnull(d.HL7MessageTime, d.InsertDate)
	join patient p
		on d.aliasname = p.aliasname
	left join PatientPayor pp
		on p.Number = pp.PatientNumber
		and pp.Type = 'P'
		--and pp.PayorNumber = '0002991'
	join IHSI_Staging st
		on p.Number = st.PatientNumber
	--left join IHSI_DemographicImportTemplateUpdatable d3
	--	on d.AliasName = d3.AliasName
	--	and d3.Imported = 1
	--	and d3.
	--left join PatientPayor pp2	--Returns 0 results
	--	on p.Number = pp2.PatientNumber
	--	and pp.Type = 'P'
	--	and pp.PayorNumber <> '0002991'
	where d.Imported = 1
		and 
		--(d.Ins1Address1 like '%875%'
		--	or d.Ins1Address1 like '%98029%'
		--	or d.Ins1Address1 like '%14610%'
		--	or d.Ins1Address1 like '%903%'
		--	or d.Ins1Address2 like '%1602%'
		--	or d.Ins1Address2 like '%61010%'
		--	or d.Ins1Address2 like '%31341%'
		--	or d.Ins1Address2 like '%7322%'
		--	or d.Ins1Address2 like '%61808%'
			--or 
			d.Ins1Address2 like '%4040%'
			--)
		and d.ServiceDate >= dateadd(month, -3, getdate())
		and d2.AliasName is null
		--and pp.PatientNumber is null
		--and d.AliasName = '4585816'
		--and pp.PayorNumber = '0000003'
	group by d.AliasName
		,pp.PayorNumber
		,d.Ins1PolicyNumber
		,d.InsertDate
		,st.VerifiedDate
		--,pp2.PayorNumber
) as PPA

declare @total float
set @total = (select count(aliasname) from #tmpPrimaryPayorAnalysis)

select *
into #tmpCounts
from
(

	select ppa.PayorNumber
		,count(ppa.payornumber) as CNT
		,@total as TotalPts
	from #tmpPrimaryPayorAnalysis ppa
	group by ppa.PayorNumber
) as TC

select tc.PayorNumber
	,tc.CNT
	,tc.TotalPts
	,Round(100 * (cast(tc.CNT as float)/tc.TotalPts), 2) as PercentOfPts
from #tmpCounts tc
order by tc.CNT desc

--select *
--into #tmpHL7AtTimeOfVer
--from
--(

--	select ppa.AliasName
--		,d.Ins1Address2
--		,d.Ins1PolicyNumber
--		,d.InsertDate
--		,ppa.VerifiedDate
--	from #tmpPrimaryPayorAnalysis ppa
--	join IHSI_DemographicImportTemplateUpdatable d
--		on ppa.AliasName = d.AliasName
--		and d.Imported = '1'
--		and d.InsertDate <= ppa.VerifiedDate
--	left join IHSI_DemographicImportTemplateUpdatable d2
--		on ppa.AliasName = d2.AliasName
--		and d2.Imported = '1'
--		and d2.InsertDate <= ppa.VerifiedDate
--	--	and d2.InsertDate > d.InsertDate
--	where ppa.PayorNumber is null
--	--	and d2.AliasName is null
--	group by ppa.AliasName
--		,d.Ins1Address2
--		,d.Ins1PolicyNumber
--		,d.InsertDate
--		,ppa.VerifiedDate
--) as TOV

--select *
--from #tmpHL7AtTimeOfVer

drop table #tmpPrimaryPayorAnalysis
drop table #tmpCounts
--drop table #tmpHL7AtTimeOfVer
