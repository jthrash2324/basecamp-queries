select *
into #tmpHL7
from (

	select d.PatientName
		,d.AliasName
		,d.MedicalRecNo
		,d.PatientBirthdate
		,d.PatientSex
		,d.PatientRace
		,d.PatientAddress1
		,d.PatientAddress2
		,d.PatientCity
		,d.PatientState
		,d.PatientZip
		,d.PatientSSN
		,d.PatientPhone1
		,d.PatientMarital
		,d.GuarantorName
		,d.GuarantorAddress1
		,d.GuarantorAddress2
		,d.GuarantorCity
		,d.GuarantorState
		,d.GuarantorZip
		,d.GuarantorSSN
		,d.GuarantorBirthdate
		,d.GuarantorSex
		,d.GuarantorRelationship
		,d.Ins1ID		--Primary
		,d.Ins1PayerName
		,d.Ins1CompanyID
		,d.Ins1Name
		,d.Ins1Address1
		,d.Ins1Address2
		,d.Ins1City
		,d.Ins1State
		,d.Ins1Zip
		,d.Ins1Phone
		,d.Ins1PolicyNumber
		,d.Ins1GroupNumber
		,d.Ins1SubscriberName
		,d.Ins1SubscriberAddress1
		,d.Ins1SubscriberAddress2
		,d.Ins1SubscriberCity
		,d.Ins1SubscriberState
		,d.Ins1SubscriberZip
		,d.Ins1SubscriberRelation
		,d.Ins1SubscriberSSN
		,d.Ins1SubscriberDOB
		,d.Ins1SubscriberSex
		,d.Ins2ID		--Secondary
		,d.Ins2PayerName
		,d.Ins2CompanyID
		,d.Ins2Name
		,d.Ins2Address1
		,d.Ins2Address2
		,d.Ins2City
		,d.Ins2State
		,d.Ins2Zip
		,d.Ins2Phone
		,d.Ins2PolicyNumber
		,d.Ins2GroupNumber
		,d.Ins2SubscriberName
		,d.Ins2SubscriberAddress1
		,d.Ins2SubscriberAddress2
		,d.Ins2SubscriberCity
		,d.Ins2SubscriberState
		,d.Ins2SubscriberZip
		,d.Ins2SubscriberRelation
		,d.Ins2SubscriberSSN
		,d.Ins2SubscriberDOB
		,d.Ins2SubscriberSex
		,d.ServiceDate
		,d.RegistrationDate
		,d.DischargeDate
		,d.ProviderNumber
		,d.AdmitDate
	from IHSI_DemographicImportTemplateUpdatable d
	left join IHSI_DemographicImportTemplateUpdatable d2
		on d.AliasName = d2.AliasName
		and isnull(d2.InsertDate, d2.HL7MessageTime) > isnull(d.InsertDate, d.HL7MessageTime)
	where d.servicedate >= DATEADD(month, -3, d.ServiceDate)
		and d2.AliasName is null
	group by d.PatientName
		,d.AliasName
		,d.MedicalRecNo
		,d.PatientBirthdate
		,d.PatientSex
		,d.PatientRace
		,d.PatientAddress1
		,d.PatientAddress2
		,d.PatientCity
		,d.PatientState
		,d.PatientZip
		,d.PatientSSN
		,d.PatientPhone1
		,d.PatientMarital
		,d.GuarantorName
		,d.GuarantorAddress1
		,d.GuarantorAddress2
		,d.GuarantorCity
		,d.GuarantorState
		,d.GuarantorZip
		,d.GuarantorSSN
		,d.GuarantorBirthdate
		,d.GuarantorSex
		,d.GuarantorRelationship
		,d.Ins1ID		--Primary
		,d.Ins1PayerName
		,d.Ins1CompanyID
		,d.Ins1Name
		,d.Ins1Address1
		,d.Ins1Address2
		,d.Ins1City
		,d.Ins1State
		,d.Ins1Zip
		,d.Ins1Phone
		,d.Ins1PolicyNumber
		,d.Ins1GroupNumber
		,d.Ins1SubscriberName
		,d.Ins1SubscriberAddress1
		,d.Ins1SubscriberAddress2
		,d.Ins1SubscriberCity
		,d.Ins1SubscriberState
		,d.Ins1SubscriberZip
		,d.Ins1SubscriberRelation
		,d.Ins1SubscriberSSN
		,d.Ins1SubscriberDOB
		,d.Ins1SubscriberSex
		,d.Ins2ID		--Secondary
		,d.Ins2PayerName
		,d.Ins2CompanyID
		,d.Ins2Name
		,d.Ins2Address1
		,d.Ins2Address2
		,d.Ins2City
		,d.Ins2State
		,d.Ins2Zip
		,d.Ins2Phone
		,d.Ins2PolicyNumber
		,d.Ins2GroupNumber
		,d.Ins2SubscriberName
		,d.Ins2SubscriberAddress1
		,d.Ins2SubscriberAddress2
		,d.Ins2SubscriberCity
		,d.Ins2SubscriberState
		,d.Ins2SubscriberZip
		,d.Ins2SubscriberRelation
		,d.Ins2SubscriberSSN
		,d.Ins2SubscriberDOB
		,d.Ins2SubscriberSex
		,d.ServiceDate
		,d.RegistrationDate
		,d.DischargeDate
		,d.ProviderNumber
		,d.AdmitDate
) as HL7

select *
into #tmpPTInfo 
from (

	select th.AliasName		--Patient tab
		,p.Number
		,th.MedicalRecNo as HL7MRN
		,p.MedicalRecordNo
		,th.PatientName as HL7PtName
		,p.Name
		,th.PatientAddress1 as HL7PtAdd1
		,p.AddressLine1
		,th.PatientAddress2 as HL7PtAdd2
		,p.AddressLine2
		,th.PatientCity as HL7PtCity
		,p.City
		,th.PatientState as HL7PtState
		,p.State
		,th.PatientZip as HL7PtZip
		,p.ZipCode
		,th.PatientPhone1 as HL7PtPhone
		,p.PhoneNo1		--Patient phone 2 also exists on verification screen
		,th.PatientSSN as HL7PtSSN
		,p.SocialSecurityNo
		,th.PatientBirthdate as HL7PtDOB
		,p.Birthdate
		,th.PatientMarital as HL7PtMarital
		,p.MaritalStatus 
		,th.PatientSex as HL7PtSex
		,p.Sex
		,th.ServiceDate as HL7DOS
		,s.FromDate
		,th.DischargeDate as HL7DischargeDate
		,st.DischargeDate
		
		,th.GuarantorName as HL7GuarName		--Guarantor Tab
		,ppg.SubscriberName
		,th.GuarantorAddress1 as HL7GuarAdd1
		,ppg.SubscriberAddress1
		,th.GuarantorAddress2 as HL7GuarAdd2
		,ppg.SubscriberAddress2
		,th.GuarantorCity as HL7GuarCity
		,ppg.SubscriberCity
		,th.GuarantorState as HL7GuarState
		,ppg.SubscriberState
		,th.GuarantorZip as HL7GuarZip
		,ppg.SubscriberZipCode
		,th.GuarantorRelationship as HL7GuarRel
		,ppg.SubscriberRelation
		,th.GuarantorBirthdate as HL7GuarDOB
		,ppg.SubscriberBirthdate
		,th.GuarantorSSN as HL7GuarSSn
		,ppg.SubscriberSocSecNo		--Gurantor employer info also exists on this tab
		,pyr1.Number as HL7PrimPayrNum		--HL7 payor info
		,pyr2.Number as HL7SecPayrNum

		,th.Ins1SubscriberName		--Primary Subscriber Info
		,th.Ins1SubscriberAddress1
		,th.Ins1SubscriberAddress2
		,th.Ins1SubscriberCity
		,th.Ins1SubscriberState
		,th.Ins1SubscriberZip
		,th.Ins1SubscriberRelation
		,th.Ins1SubscriberDOB
		,th.Ins1SubscriberSSN
		,th.Ins1SubscriberSex		--Coverage dates not included

		,th.Ins2SubscriberName		--Secondary Subscriber Info
		,th.Ins2SubscriberAddress1
		,th.Ins2SubscriberAddress2
		,th.Ins2SubscriberCity
		,th.Ins2SubscriberState
		,th.Ins2SubscriberZip
		,th.Ins2SubscriberRelation
		,th.Ins2SubscriberDOB
		,th.Ins2SubscriberSSN
		,th.Ins2SubscriberSex		--Coverage dates not included
	from #tmpHL7 th
	left join patient p
		on th.AliasName = p.AliasName
	left join service s
		on p.Number = s.PatientNumber
	left join IHSI_Staging st
		on p.Number = st.PatientNumber
	left join PatientPayor ppg	--Guarantor line
		on p.Number = ppg.PatientNumber
		and ((ppg.Type = 'P' and ppg.PayorNumber = '0000001')	or ppg.Type = 'G')	--Modified 1/24/2019
	left join IHSI_PayorTranslation ptp	--Primary payor
		on th.Ins1ID = ptp.ins_id
	left join payor pyr1
		on ptp.PayorNumber = pyr1.Number
	left join IHSI_PayorTranslation pts	--Secondary Payor
		on th.Ins2ID = pts.ins_id
	left join Payor pyr2
		on pts.PayorNumber = pyr2.Number
	group by th.AliasName		--Patient tab
		,p.Number
		,th.MedicalRecNo
		,p.MedicalRecordNo
		,th.PatientName
		,p.Name
		,th.PatientAddress1
		,p.AddressLine1
		,th.PatientAddress2
		,p.AddressLine2
		,th.PatientCity
		,p.City
		,th.PatientState
		,p.State
		,th.PatientZip
		,p.ZipCode
		,th.PatientPhone1
		,p.PhoneNo1		--Patient phone 2 also exists on verification screen
		,th.PatientSSN
		,p.SocialSecurityNo
		,th.PatientBirthdate
		,p.Birthdate
		,th.PatientMarital
		,p.MaritalStatus
		,th.PatientSex
		,p.Sex
		,th.ServiceDate
		,s.FromDate
		,th.DischargeDate
		,st.DischargeDate
		
		,th.GuarantorName		--Guarantor Tab
		,ppg.SubscriberName
		,th.GuarantorAddress1
		,ppg.SubscriberAddress1
		,th.GuarantorAddress2
		,ppg.SubscriberAddress2
		,th.GuarantorCity
		,ppg.SubscriberCity
		,th.GuarantorState
		,ppg.SubscriberState
		,th.GuarantorZip
		,ppg.SubscriberZipCode
		,th.GuarantorRelationship
		,ppg.SubscriberRelation
		,th.GuarantorBirthdate
		,ppg.SubscriberBirthdate
		,th.GuarantorSSN
		,ppg.SubscriberSocSecNo		--Gurantor employer info also exists on this tab
		,pyr1.Number	--HL7 payor info
		,pyr2.Number 
		
		,th.Ins1SubscriberName		--Primary Subscriber Info
		,th.Ins1SubscriberAddress1
		,th.Ins1SubscriberAddress2
		,th.Ins1SubscriberCity
		,th.Ins1SubscriberState
		,th.Ins1SubscriberZip
		,th.Ins1SubscriberRelation
		,th.Ins1SubscriberDOB
		,th.Ins1SubscriberSSN
		,th.Ins1SubscriberSex		--Coverage dates not included

		,th.Ins2SubscriberName		--Secondary Subscriber Info
		,th.Ins2SubscriberAddress1
		,th.Ins2SubscriberAddress2
		,th.Ins2SubscriberCity
		,th.Ins2SubscriberState
		,th.Ins2SubscriberZip
		,th.Ins2SubscriberRelation
		,th.Ins2SubscriberDOB
		,th.Ins2SubscriberSSN
		,th.Ins2SubscriberSex		--Coverage dates not included
) tpi

select *
into #tmpAll
from
(

	select tpi.AliasName		--Patient tab
		,tpi.Number
		,tpi.HL7MRN
		,tpi.MedicalRecordNo
		,tpi.HL7PtName
		,tpi.Name
		,tpi.HL7PtAdd1
		,tpi.AddressLine1
		,tpi.HL7PtAdd2
		,tpi.AddressLine2
		,tpi.HL7PtCity
		,tpi.City
		,tpi.HL7PtState
		,tpi.State
		,tpi.HL7PtZip
		,tpi.ZipCode
		,tpi.HL7PtPhone
		,tpi.PhoneNo1		--Patient phone 2 also exists on verification screen
		,tpi.HL7PtSSN
		,tpi.SocialSecurityNo
		,tpi.HL7PtDOB
		,tpi.Birthdate
		,tpi.HL7PtMarital
		,tpi.MaritalStatus 
		,tpi.HL7PtSex
		,tpi.Sex
		,tpi.HL7DOS
		,tpi.FromDate
		,tpi.HL7DischargeDate
		,tpi.DischargeDate
		
		,tpi.HL7GuarName		--Guarantor Tab
		,tpi.SubscriberName
		,tpi.HL7GuarAdd1
		,tpi.SubscriberAddress1
		,tpi.HL7GuarAdd2
		,tpi.SubscriberAddress2
		,tpi.HL7GuarCity
		,tpi.SubscriberCity
		,tpi.HL7GuarState
		,tpi.SubscriberState
		,tpi.HL7GuarZip
		,tpi.SubscriberZipCode
		,tpi.HL7GuarRel
		,tpi.SubscriberRelation
		,tpi.HL7GuarDOB
		,tpi.SubscriberBirthdate
		,tpi.HL7GuarSSn
		,tpi.SubscriberSocSecNo		--Gurantor employer info also exists on tpiis tab
		,tpi.HL7PrimPayrNum		--HL7 payor info
		,tpi.HL7SecPayrNum




		,tpi.Ins1SubscriberName		--Primary Subscriber Info
		,pp1.SubscriberName as PSubName
		,tpi.Ins1SubscriberAddress1
		,pp1.SubscriberAddress1 as PSubAdd1
		,tpi.Ins1SubscriberAddress2 
		,pp1.SubscriberAddress2 as PSubAdd2
		,tpi.Ins1SubscriberCity
		,pp1.SubscriberCity as PSubCity
		,tpi.Ins1SubscriberState
		,pp1.SubscriberState as PSubState
		,tpi.Ins1SubscriberZip
		,pp1.SubscriberZipCode as PSubZip
		,tpi.Ins1SubscriberRelation
		,pp1.SubscriberRelation as PSubRel
		,tpi.Ins1SubscriberDOB
		,pp1.SubscriberBirthdate as PSubDob
		,tpi.Ins1SubscriberSSN
		,pp1.SubscriberSocSecNo  as PSubSSN
		,tpi.Ins1SubscriberSex		--Coverage dates not included
		,pp1.SubscriberSex as PSubSex

		,tpi.Ins2SubscriberName		--Secondary Subscriber Info
		,pp2.SubscriberName as SSubName
		,tpi.Ins2SubscriberAddress1
		,pp2.SubscriberAddress1 as SSubAdd1
		,tpi.Ins2SubscriberAddress2
		,pp2.SubscriberAddress2 as SSubAdd2
		,tpi.Ins2SubscriberCity
		,pp2.SubscriberCity as SSubCity
		,tpi.Ins2SubscriberState
		,pp2.SubscriberState as SSubState
		,tpi.Ins2SubscriberZip
		,pp2.SubscriberZipCode as SSubZip
		,tpi.Ins2SubscriberRelation
		,pp2.SubscriberRelation as SSubRel
		,tpi.Ins2SubscriberDOB
		,pp2.SubscriberBirthdate as SSubDOB
		,tpi.Ins2SubscriberSSN
		,pp2.SubscriberSocSecNo as SSubSSN
		,tpi.Ins2SubscriberSex		--Coverage dates not included
		,pp2.SubscriberSex as SSubSex






from #tmpPTInfo tpi
left join PatientPayor pp1
	on tpi.Number = pp1.PatientNumber
	and tpi.HL7PrimPayrNum = pp1.PayorNumber
	and pp1.Type = 'P'
left join PatientPayor pp2
	on tpi.Number = pp2.PatientNumber
	and tpi.HL7SecPayrNum = pp2.PayorNumber
	and pp2.Type = 'S'
group by tpi.AliasName		--Patient tab
		,tpi.Number
		,tpi.HL7MRN
		,tpi.MedicalRecordNo
		,tpi.HL7PtName
		,tpi.Name
		,tpi.HL7PtAdd1
		,tpi.AddressLine1
		,tpi.HL7PtAdd2
		,tpi.AddressLine2
		,tpi.HL7PtCity
		,tpi.City
		,tpi.HL7PtState
		,tpi.State
		,tpi.HL7PtZip
		,tpi.ZipCode
		,tpi.HL7PtPhone
		,tpi.PhoneNo1		--Patient phone 2 also exists on verification screen
		,tpi.HL7PtSSN
		,tpi.SocialSecurityNo
		,tpi.HL7PtDOB
		,tpi.Birthdate
		,tpi.HL7PtMarital
		,tpi.MaritalStatus 
		,tpi.HL7PtSex
		,tpi.Sex
		,tpi.HL7DOS
		,tpi.FromDate
		,tpi.HL7DischargeDate
		,tpi.DischargeDate
		
		,tpi.HL7GuarName		--Guarantor Tab
		,tpi.SubscriberName
		,tpi.HL7GuarAdd1
		,tpi.SubscriberAddress1
		,tpi.HL7GuarAdd2
		,tpi.SubscriberAddress2
		,tpi.HL7GuarCity
		,tpi.SubscriberCity
		,tpi.HL7GuarState
		,tpi.SubscriberState
		,tpi.HL7GuarZip
		,tpi.SubscriberZipCode
		,tpi.HL7GuarRel
		,tpi.SubscriberRelation
		,tpi.HL7GuarDOB
		,tpi.SubscriberBirthdate
		,tpi.HL7GuarSSn
		,tpi.SubscriberSocSecNo		--Gurantor employer info also exists on tpiis tab
		,tpi.HL7PrimPayrNum		--HL7 payor info
		,tpi.HL7SecPayrNum


				,tpi.Ins1SubscriberName		--Primary Subscriber Info
		,pp1.SubscriberName
		,tpi.Ins1SubscriberAddress1
		,pp1.SubscriberAddress1
		,tpi.Ins1SubscriberAddress2
		,pp1.SubscriberAddress2
		,tpi.Ins1SubscriberCity
		,pp1.SubscriberCity
		,tpi.Ins1SubscriberState
		,pp1.SubscriberState
		,tpi.Ins1SubscriberZip
		,pp1.SubscriberZipCode
		,tpi.Ins1SubscriberRelation
		,pp1.SubscriberRelation
		,tpi.Ins1SubscriberDOB
		,pp1.SubscriberBirthdate
		,tpi.Ins1SubscriberSSN
		,pp1.SubscriberSocSecNo
		,tpi.Ins1SubscriberSex		--Coverage dates not included
		,pp1.SubscriberSex

		,tpi.Ins2SubscriberName		--Secondary Subscriber Info
		,pp2.SubscriberName
		,tpi.Ins2SubscriberAddress1
		,pp2.SubscriberAddress1
		,tpi.Ins2SubscriberAddress2
		,pp2.SubscriberAddress2
		,tpi.Ins2SubscriberCity
		,pp2.SubscriberCity
		,tpi.Ins2SubscriberState
		,pp2.SubscriberState
		,tpi.Ins2SubscriberZip
		,pp2.SubscriberZipCode
		,tpi.Ins2SubscriberRelation
		,pp2.SubscriberRelation
		,tpi.Ins2SubscriberDOB
		,pp2.SubscriberBirthdate
		,tpi.Ins2SubscriberSSN
		,pp2.SubscriberSocSecNo
		,tpi.Ins2SubscriberSex		--Coverage dates not included
		,pp2.SubscriberSex

) as TA


--/*PatientName*/
--select count(*)
--from #tmpAll ta
--where ta.number is not null
--	----and ta.hl7mrn <> ta.medicalrecordno
--	and replace(replace(ta.hl7ptname, ', ', ','), '  ', ' ') = ta.name	--Replace whitespace after comma and then any double whitespaces with single
--	and ta.hl7ptname not like '%patient%'	--Filter out placeholders to manually verify
--	--and ta.name like '%III%'
----order by ta.aliasname desc
--------------------------------------------------

--/*Patient Address 1*/
--select count(*)
--from #tmpAll ta
--where ta.number is not null
--	and ta.HL7PtAdd1 = ta.AddressLine1
--	and ta.HL7PtAdd1 not like '%Bad%'	--Review manually
----order by ta.aliasname desc
----------------------------------------------------

--/*Patient Address 2*/
--select count(*)
--from #tmpAll ta
--where ta.number is not null
--	and ta.HL7PtAdd2 <> ta.AddressLine2
--	and ta.HL7PtAdd2 not like '%Bad%'	--Review manually
----order by ta.aliasname desc
----------------------------------------------------

--/*Patient City*/
--select count(*)
--from #tmpAll ta
--where ta.number is not null
--	and ta.HL7PtCity <> ta.City
--	and ta.HL7PtCity not like '%Bad%'	--Review manually
----order by ta.aliasname desc
----------------------------------------------------

--/*Patient State*/
--select count(*)
--from #tmpAll ta
--where ta.number is not null
--	and ta.HL7PtState <> ta.State
--	--and ta.HL7PtCity not like '%Bad%'	--Review manually
----order by ta.aliasname desc
----------------------------------------------------

select case when ta.HL7MRN <> ta.MedicalRecordNo then 1 else 0 end as MRNMismatch
	,case when replace(replace(ta.hl7ptname, ', ', ','), '  ', ' ') <> ta.name then 1 else 0 end as NameMismatch		--Patient name needs (some) whitespace removed 
	,case when ta.HL7PtAdd1 <> ta.AddressLine1 then 1 else 0 end as PtAdd1Mismatch
	,case when ISNULL(ta.HL7PtAdd2, '') <> ta.AddressLine2 then 1 else 0 end as PtAdd2Mismatch
	,case when ta.HL7PtCity <> ta.City then 1 else 0 end as PtCityMismatch
	,case when ta.HL7PtState <> ta.State then 1 else 0 end as PtCityMismatch
	,case when ta.HL7PtZip + replicate('0', 9-LEN(ta.HL7PtZip)) <> ta.ZipCode then 1 else 0 end as PtZipMismatch	--Patient zip needs trailing zeros added
	,case when ta.HL7PtPhone <> ta.PhoneNo1 then 1 else 0 end as PtPhoneMismatch
	,case when (REPLACE(ta.HL7PtSSN, '-', '') <> ta.SocialSecurityNo and ta.SocialSecurityNo <> '999999999') then 1	else 0 end as PtSSNMismatch--Patient ssn needs dashes stripped
	,case when cast(ta.HL7PtDOB as datetime) <> ta.Birthdate then 1	else 0 end as PtDOBMismatch--Patient DOB needs to be cast to datetime
	,case when ta.HL7PtMarital <> ta.MaritalStatus then 1 else 0 end as PtMaritalMismatch
	,case when ta.HL7PtSex <> Sex then 1 else 0 end as PtSexMismatch
	,case when cast(cast(ta.HL7DOS as date) as smalldatetime) <> ta.FromDate then 1 else 0 end as DOSMismatch	--DOS needs seconds stripped
	,case when cast(ta.HL7DischargeDate as date) <> cast(ta.DischargeDate as date) then 1 else 0 end as DischargeDateMismatch	--Timestamp seems to be random but does this field really matter?
	,case when replace(replace(ta.HL7GuarName, ', ', ','), '  ', ' ') <> ta.SubscriberName then 1 else 0 end as GuarNameMismatch	--Once again, name needs whitespace removed. Logic with the guarrel ans sub rel may be useful
	,case when ta.HL7GuarAdd1 <> ta.SubscriberAddress1 then 1 else 0 end as GuarAdd1Mismatch
	,case when ISNULL(ta.HL7GuarAdd2, '') <> ta.SubscriberAddress2 then 1 else 0 end as GuarAdd2Mismatch
	,case when ta.HL7GuarCity <> ta.SubscriberCity then 1 else 0 end as GuarCityMismatch --Some of these just start with a ' '
	,case when ta.HL7GuarZip + replicate('0', 9-LEN(ta.HL7GuarZip)) <> ta.SubscriberZipCode then 1 else 0 end as GuarZipMismatch	--Once again, format zipcode
	,case when ta.HL7GuarRel = '' then 'Missing' else 'Present' end as HL7GuarRelAvailibility	--Field is missing most of the time and may need to be added by client
	,case when (ta.HL7GuarRel <> ta.SubscriberRelation and ta.HL7GuarRel <> '') then 1 else 0 end as GuarRelMismatch
	,case when cast(ta.HL7GuarDOB as datetime) <> ta.SubscriberBirthdate then 1 else 0 end as GuarDOBMismatch
	,case when (REPLACE(ta.HL7GuarSSn, '-', '') <> ta.SubscriberSocSecNo and ta.SubscriberSocSecNo <> '999999999') then 1	else 0 end as GuarSSNMismatch--Patient ssn needs dashes stripped
	,case when ta.HL7PrimPayrNum is null then 1 else 0 end as PrimPyrMissOrMismatch
	,*
from #tmpAll ta
where ta.Number is not null	--Patients not imported 
	and ta.HL7PtName not like '%patient%'	--Placeholder for name (MANUAL REVIEW)
	and ta.HL7PtAdd1 not like '%bad%'	--Placeholder for address (MANUAL REVIEW)
	and ta.AddressLine2 not like '%return%'	--Mail was returned so patient record was updated
	and ta.HL7PtSSN <> '000-00-0000'	--Placeholder for SSN (MANUAL REVIEW)
	and ta.HL7GuarName is not null	--Can these be replaced by patient info? (MANUAL REVIEW)
	and (datediff(day, ta.HL7PtDOB, ta.HL7DOS) >= 6574 or ta.HL7PtName <> ta.HL7GuarName)	--Minor listed as guarantor (MANUAL REVIEW)
	and ta.HL7GuarSSn <> '000-00-0000' --Placeholder (MANUAL REVIEW)
	and ta.HL7PrimPayrNum is not null --No matching payor billed, investigate mapping then (MANUAL REVIEW)


--drop table #tmpHL7
--drop table #tmpPTInfo
--drop table #tmpAll