select *
into #tmpAddress
from
(

	select d.aliasname
		,d.Ins1CompanyID
		,d.Ins1ID
		,d.Ins1Address1
		,d.Ins1Address2
	from IHSI_DemographicImportTemplateUpdatable d
	left join IHSI_DemographicImportTemplateUpdatable d2
		on d.AliasName = d2.AliasName
		and d2.Imported = '1'
		and d2.InsertDate > d.InsertDate
	where d.ServiceDate >= dateadd(month, -3, getdate())
		and d.Imported = 1
		and d2.AliasName is null
		and d.Ins1Address1 is not null
	group by d.AliasName
		,d.Ins1CompanyID
		,d.Ins1ID
		,d.Ins1Address1
		,d.Ins1Address2
) as TA

select ta.Ins1Address1
	,ta.Ins1Address2
	,ta.Ins1CompanyID
	,ta.Ins1ID
	,count(ta.AliasName) as CNT
from #tmpAddress ta
group by ta.Ins1Address1
	,ta.Ins1Address2
	,ta.Ins1CompanyID
	,ta.Ins1ID
order by CNT desc

drop table #tmpAddress

select *
from IHSI_PayorTranslation
--where ins_id = 'MCR'
order by ins_id asc


--select Ins1ID
--	,Ins1Address1
--	,Ins1Address2
--	,count(distinct aliasname) as cnt
--from IHSI_DemographicImportTemplateUpdatable
--where Ins1id = 'MCR'
--group by Ins1ID
--	,Ins1Address1
--	,Ins1Address2
--order by cnt desc