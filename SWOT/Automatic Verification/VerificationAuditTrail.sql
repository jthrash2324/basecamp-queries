select *
into VerificationAuditToBeDeleted
from
(
	select distinct p.AliasName		------Patient Tab
		,p.MedicalRecordNo
		,p.Name
		,p.Number
		,st.Staging_ID
		,p.AddressLine1
		,p.AddressLine2
		,p.City
		,p.State
		,p.ZipCode
		,p.PhoneNo1
		,p.PhoneNo2
		,p.SocialSecurityNo
		,p.MaritalStatus
		,p.Sex
		,st.TI_DateOfService
		,st.DischargeDate
	
		,pp.SubscriberName as GuarName		-----Guarantor tab
		,pp.SubscriberAddress1 as GuarAdd1
		,pp.SubscriberAddress2 as GuarAdd2
		,pp.SubscriberCity as GuarCity 
		,pp.SubscriberState as GuarState
		,pp.SubscriberZipCode as GuarZip
		,pp.SubscriberPhoneNo as GuarPhone
		,pp.SubscriberRelation as GuarRel
		,pp.SubscriberBirthdate as GuarDOB
		,pp.SubscriberSocSecNo as GuarSSn
		,pp.SubscriberSex as GuarSex
		,pp.EmployerName as GuarEmp	----Note: Locate employer information for all tabs
	
		--,ptp.Description		------Payor 1 tab
		,ppp.PayorNumber as PPyrNum
		--,pyrp.Name
		,st.ProviderNumber
		,ppp.PolicyNumber as PPolNum
		,ppp.GroupNumber as PGrpNum
		,ppp.SubscriberName as PSubName
		,ppp.SubscriberAddress1 as PSubAdd1
		,ppp.SubscriberAddress2 as PSubAdd2
		,ppp.SubscriberCity as PSubCity
		,ppp.SubscriberState as PSubState
		,ppp.SubscriberZipCode as PSubZip
		,ppp.SubscriberPhoneNo as PSubPhone
		,ppp.SubscriberRelation as PSubRel
		,ppp.SubscriberBirthdate as PSubDOB
		,ppp.SubscriberSocSecNo as PSubSSN
		,ppp.SubscriberSex as PSubSex
		,ppp.EmployerName as PSubEmp
		,ppp.DateCoverageBegins as PSubCovStart
		,ppp.DateCoverageEnds as PSubCovEnd
		,ppp.InsuranceType as PSubInsType
	
		,pps.PayorNumber
		--,pyrp.Name
		--,st.ProviderNumber
		,pps.PolicyNumber
		,pps.GroupNumber
		,pps.SubscriberName
		,pps.SubscriberAddress1
		,pps.SubscriberAddress2
		,pps.SubscriberCity
		,pps.SubscriberState
		,pps.SubscriberZipCode
		,pps.SubscriberPhoneNo
		,pps.SubscriberRelation
		,pps.SubscriberBirthdate
		,pps.SubscriberSocSecNo
		,pps.SubscriberSex
		,pps.EmployerName
		,pps.DateCoverageBegins
		,pps.DateCoverageEnds
		,pps.InsuranceType			---Payor 2 tab
	from IHSI_Staging st
	join patient p
		on st.PatientNumber = p.Number
	left join patientpayor pp	
		on st.patientnumber = pp.PatientNumber
		and pp.Type = 'G'	--Guarantor line
	left join PatientPayor ppp
		on st.PatientNumber = ppp.PatientNumber
		and ppp.Type = 'P' --Primary payor
	--left join Payor pyrp
	--	on ppp.PayorNumber = pyrp.Number
	--left join IHSI_PayorType ptp
	--	on pyrp.TypeCode = ptp.Code
	left join PatientPayor pps
		on st.PatientNumber = pps.PatientNumber
		and pps.Type = 'S' --Secondary payor
	join IHSI_DemographicImportTemplateUpdatable d	--Only look at records that are actually in the demo table
		on p.AliasName = d.AliasName
	where st.Verified = 'N'
) A

select *
from VerificationAuditToBeDeleted

--drop table VerificationAuditToBeDeleted