USE [LA_WCCH_CCEPG]
GO
/****** Object:  StoredProcedure [dbo].[IHSI_ClinVerificationQueueByHoursQueue]    Script Date: 12/6/2018 9:14:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[IHSI_ClinVerificationQueueByHoursQueue]
	
AS

/*
-- =============================================
-- Author:		Sriram.C
-- Create date: 09-MAY-2017
-- Description:	Created for the modified ICIS CL Verification Screen in version 1.1.1.100 per SR 40179. It takes the user to the most recent chart based on BCD date
--				when the user clicks 'Verify Available Charts'. It also populates the verification screen when the user checks 'Show Queue'.
-- Note:		
-- Entity Type: CL Verification Queue
--
-- Last Modified:
	- 12/3/2018 by Justin. We need to increase our client delays to make time for Eligibill responses and linking so I put a 24 hour delay on things entereing the queue to be implemented client by client with the DB_Name in clause.

	- 6/19/17 by Justin T per Kurstyn L. on Zendesk 44749. Added three conditions to the where clause to avoid the same record showing up in the queue for each date of service.
	
	- 6/8/2017 by Sriram C.
	
	- 5/31/2017 by Justin T. 
-- =============================================
IHSI_ClinVerificationQueueByHoursQueue
*/

IF DB_NAME ()IN 
('IL_AGSOC_DEPCC')

BEGIN

		SET NOCOUNT ON;

	select
		st.summary_id,
		Case
			WHEN MAX(i.StagedDate)>st.stagedDate THEN MAX(i.StagedDate)
			WHEN MAX(i.StagedDate)<=st.stagedDate THEN MAX(st.StagedDate)
		END AS MostRecentDate,
		dr.ti_dateofservice, 
		i.ti_accountnumber, ISNULL(p1.typecode, 'XX') AS PayorType, 
					'NA' as SkipReason, 'NA' as StatusCheck, st.PatientNumber	
	from IHSI_ClinSummaryRecord as st
		LEFT JOIN  IHSI_ClinImageFiles as i on st.summary_id=i.summary_id
		LEFT JOIN dbo.PatientPayor as PP1 on pp1.patientnumber = st.patientnumber AND pp1.type   = 'P' 
		LEFT JOIN dbo.PatientPayor as PP2 ON (pp1.patientnumber = pp2.patientnumber AND		(pp1.DateCoverageBegins < pp2.DateCoverageBegins  OR (pp1.DateCoverageBegins is NULL	 and pp2.DateCoverageBegins is not null)) AND pp2.Type = 'P')
		LEFT JOIN dbo.Payor as P1 on P1.number = pp1.payornumber
		LEFT JOIN dbo.IHSI_PayorTypeQueueOrder as PT on ISNULL(p1.typecode, 'XX') =		PT.PayorType AND PT.Screen = 'Verification'
		LEFT JOIN dbo.IHSI_ClinDetailRecord as dr on i.Summary_ID = dr.Summary_ID
		LEFT JOIN dbo.IHSI_ClinDetailRecord as dr2 on dr.Summary_ID = dr2.Summary_ID AND	(dr.TI_DateOfService > dr2.TI_DateOfService)	
	WHERE ST.SKIPPED='N'
		AND ST.Verified = 'N'
		AND ST.VerifyLock = 'N'
		
		and st.summary_id in (select i.summary_id from dbo.IHSI_ClinImageFiles as i)
		AND pp2.PatientNumber is null
		AND dr2.Detail_ID is null
		and i.StagedDate <= DATEADD(hour,-24,getdate())	--Added 12/3/18 by Justin to delay for ELGB files
		
	Group by st.StagedDate,st.summary_id,
	dr.ti_dateofservice, 
	i.ti_accountnumber, ISNULL(p1.typecode, 'XX'),st.PatientNumber
	having COUNT(i.image_id)>=1
	order by MostRecentDate asc			
	
END

ELSE

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--select
--	st.summary_id as staging_id,
--	Case
--		WHEN MAX(i.StagedDate)>st.stagedDate THEN MAX(i.StagedDate)
--		WHEN MAX(i.StagedDate)<=st.stagedDate THEN MAX(st.StagedDate)
--		END AS MostRecentDate

--from IHSI_ClinSummaryRecord as st
--	left join IHSI_ClinImageFiles as i on st.summary_id=i.summary_id
--WHERE ST.SKIPPED='N'
--	AND ST.Verified = 'N'
--	AND ST.VerifyLock = 'N'
--Group by st.StagedDate,st.summary_id
--having COUNT(i.image_id)>=1
--order by MostRecentDate asc	
		
	select
		st.summary_id,
		Case
			WHEN MAX(i.StagedDate)>st.stagedDate THEN MAX(i.StagedDate)
			WHEN MAX(i.StagedDate)<=st.stagedDate THEN MAX(st.StagedDate)
		END AS MostRecentDate,
		dr.ti_dateofservice, 
		i.ti_accountnumber, ISNULL(p1.typecode, 'XX') AS PayorType, 
					'NA' as SkipReason, 'NA' as StatusCheck, st.PatientNumber	
	from IHSI_ClinSummaryRecord as st
		LEFT JOIN  IHSI_ClinImageFiles as i on st.summary_id=i.summary_id
		LEFT JOIN dbo.PatientPayor as PP1 on pp1.patientnumber = st.patientnumber AND pp1.type   = 'P' 
		LEFT JOIN dbo.PatientPayor as PP2 ON (pp1.patientnumber = pp2.patientnumber AND		(pp1.DateCoverageBegins < pp2.DateCoverageBegins  OR (pp1.DateCoverageBegins is NULL	 and pp2.DateCoverageBegins is not null)) AND pp2.Type = 'P')
		LEFT JOIN dbo.Payor as P1 on P1.number = pp1.payornumber
		LEFT JOIN dbo.IHSI_PayorTypeQueueOrder as PT on ISNULL(p1.typecode, 'XX') =		PT.PayorType AND PT.Screen = 'Verification'
		LEFT JOIN dbo.IHSI_ClinDetailRecord as dr on i.Summary_ID = dr.Summary_ID
		LEFT JOIN dbo.IHSI_ClinDetailRecord as dr2 on dr.Summary_ID = dr2.Summary_ID AND	(dr.TI_DateOfService > dr2.TI_DateOfService)	
	WHERE ST.SKIPPED='N'
		AND ST.Verified = 'N'
		AND ST.VerifyLock = 'N'
		
		and st.summary_id in (select i.summary_id from dbo.IHSI_ClinImageFiles as i)
		AND pp2.PatientNumber is null
		AND dr2.Detail_ID is null
		
	Group by st.StagedDate,st.summary_id,
	dr.ti_dateofservice, 
	i.ti_accountnumber, ISNULL(p1.typecode, 'XX'),st.PatientNumber
	having COUNT(i.image_id)>=1
	order by MostRecentDate asc			
	
END
