select d1.AliasName
	,cast(d1.ServiceDate as date) as DOS
	,case
		when d2.AliasName is not null then 'Y'
		else 'N'
	end as A03Received
from IHSI_DemographicImportTemplateUpdatable d1
left join IHSI_DemographicImportTemplateUpdatable d2
	on d1.AliasName = d2.AliasName
	and d2.MessageType = 'ADT^A03'
where d1.MessageType <> 'ADT^A03'
	and d1.MessageType is not null
	and d1.ServiceDate >= '2018-10-01'
	and d1.Imported = 1
	and d2.AliasName is null
group by d1.AliasName ,
	cast(d1.ServiceDate as date)
	,d2.AliasName
order by dos desc