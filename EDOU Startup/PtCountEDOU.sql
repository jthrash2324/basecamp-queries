select count(distinct d.AliasName) as PTCountEDOU
	,cast(d.ServiceDate as date) as DOS
from IHSI_DemographicImportTemplateUpdatable d
where d.Imported = 1
group by cast(d.ServiceDate as date)
order by DOS desc