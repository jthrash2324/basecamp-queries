select cast(s.FromDate as date) as BilledDOS
	,p.MedicalRecordNo
	,case
		when d.MedicalRecNo is not null then 'Y'
		else 'N'
	end as DOSMatch
from service s
join patient p
	on s.PatientNumber = p.number
left join IHSI_DemographicImportTemplateUpdatable d
	on p.MedicalRecordNo = d.MedicalRecNo
	and d.MessageType = 'ADT^A03'
where s.amount > 0.01
	and d.MedicalRecNo is null
group by cast(s.FromDate as date)
	,p.MedicalRecordNo
	,d.MedicalRecNo
