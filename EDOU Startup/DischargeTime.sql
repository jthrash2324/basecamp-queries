select *
into #tmpHL7
from
(
	select d1.AliasName
		,cast(d1.ServiceDate as date) as DOS
		,cast(min(d1.HL7MessageTime) as date) as InitialMessageDate
		,cast(min(d2.hl7messagetime) as date) as DischargeMessageDate
		,datediff(day,cast(min(d1.HL7MessageTime) as date),cast(min(d2.hl7messagetime) as date)) as DaysTillDischarge
	from IHSI_DemographicImportTemplateUpdatable d1
	join IHSI_DemographicImportTemplateUpdatable d2
		on d1.AliasName = d2.AliasName
		and d2.MessageType is not null
		and d2.MessageType = 'ADT^A03'
	where d1.ServiceDate >= '1993-03-22'
		and d1.MessageType is not null
		and d1.MessageType <> 'ADT^A03'
	group by d1.AliasName
		,cast(d1.ServiceDate as date)
	--order by DaysTillDischarge desc
) as x

select t.DaysTillDischarge
	,count(t.AliasName) 
from #tmpHL7 t
group by t.DaysTillDischarge
order by t.DaysTillDischarge desc

drop table #tmpHL7