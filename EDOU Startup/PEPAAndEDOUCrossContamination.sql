select 
	d.aliasname
	,d.MedicalRecNo
	,cast(d.servicedate as date) as DOS
	,cast(d.HL7MessageTime as Date) as HL7Date 
	,case
		when p.AliasName is not null then 'Y'
		else 'N'
	end as PatientInOLOLDB
	,case
		when i.TI_AccountNumber is not null then 'Y'
		else 'N'
	end as ChartInOLOLDB
	,case
		when ci.TI_AccountNumber is not null then 'Y'
		else 'N'
	end as ChartInEDOUDB
	,'Y' as PatientInEDOUDB
	,'' as CorrectDB
from IHSI_DemographicImportTemplateUpdatable d
left join LA_OLOL_PEPA.dbo.Patient p
	on d.AliasName = p.AliasName
left join LA_OLOL_PEPA.dbo.IHSI_ImageFiles i
	on p.AliasName = i.TI_AccountNumber
	and i.ImageFileName not like '%dem%'
	and i.ImageFileName not like '%nsbep%'
	and i.ImageFileName not like '%elgb%'
left join IHSI_ClinImageFiles ci
	on d.MedicalRecNo = SUBSTRING(ci.ImageFileName, CHARINDEX('MRN-', ci.ImageFileName) + 4, CHARINDEX('_CSN', ci.ImageFileName) - (CHARINDEX('MRN-', ci.ImageFileName) + 4))
where d.Imported = 1
group by 	d.aliasname
	,d.MedicalRecNo
	,cast(d.servicedate as Date)
	,cast(d.HL7MessageTime as Date)
	,i.TI_AccountNumber
	,ci.TI_AccountNumber
	,p.AliasName
order by dos asc