select d1.MedicalRecNo
	,cast(d1.ServiceDate as date) as DemoDOS
	,cast(s.FromDate as date) as BilledDOS
from service s
join patient p
	on s.PatientNumber = p.Number
join IHSI_DemographicImportTemplateUpdatable d1
	on p.MedicalRecordNo = d1.MedicalRecNo
	and cast(s.FromDate as date) = cast(d1.ServiceDate as date) 
	and d1.messagetype <> 'ADT^A03'
left join IHSI_DemographicImportTemplateUpdatable d2
	on d1.AliasName = d2.AliasName
	and d2.MessageType = 'ADT^A03'
left join Adjustment a
	on s.Number = a.ServiceNumber
	and a.AdjustmentTypeCode = '019'
where s.amount * s.Units > 0.01
	and d2.AliasName is null
	and s.TransToSvcNum = ''
	and a.ServiceNumber is null
	and s.FromDate >= dateadd(day,-14,getdate())
group by d1.MedicalRecNo
	,cast(d1.ServiceDate as date)
	,cast(s.FromDate as date)
order by cast(s.FromDate as date) desc

--select *
--from IHSI_DemographicImportTemplateUpdatable
--where MedicalRecNo in (
--'2156921'
--,'570506'
--)