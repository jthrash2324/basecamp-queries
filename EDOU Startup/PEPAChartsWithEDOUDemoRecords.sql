use LA_OLOL_PEPA

select count(distinct p.AliasName) as OLOLPatientsWithChartsWithDemsInEDOU
	--,p.MedicalRecordNo
	,cast(d.servicedate as date) as DOS
	--,i.ImageFileName
	--,st.Coded
	--,st.FollowUpRequired
	--,st.Staging_ID
from patient p
join LA_EDOU_PEPA.dbo.IHSI_DemographicImportTemplateUpdatable d
	on p.AliasName = d.AliasName
join IHSI_ImageFiles i
	on p.AliasName = i.TI_AccountNumber
	and i.ImageFileName not like '%dem%'
	and i.ImageFileName not like '%elgb%'
	and i.ImageFileName not like '%nsbep%'
join IHSI_Staging st
	on i.Staging_ID = st.Staging_ID

where d.ServiceDate >= '2018-11-06'
group by --p.AliasName
	--,p.MedicalRecordNo
	--,
	cast(d.servicedate as date)
	--,i.ImageFileName
	--,st.Coded
	--,st.FollowUpRequired
	--,st.Staging_ID
order by dos desc