select d.AliasName
	,d.MedicalRecNo
	,cast(d.ServiceDate as date) as DOS
	,d.Ins1Name
	,pay.Name as PrimaryPayor
	,d.Ins2Name
	,pays.Name as SecondaryPayor
from IHSI_DemographicImportTemplateUpdatable d
left join IHSI_DemographicImportTemplateUpdatable d2
	on d.AliasName = d2.AliasName
	and d2.HL7MessageTime > d.HL7MessageTime
join patient p
	on d.MedicalRecNo = p.MedicalRecordNo
left join PatientPayor pp
	on p.Number = pp.PatientNumber
	and pp.Type = 'P'
left join PatientPayor pps
	on p.Number = pps.PatientNumber
	and pps.Type = 'S'
left join payor pay
	on pp.PayorNumber = pay.Number
left join payor pays
	on pps.PayorNumber = pays.Number
where d.ServiceDate >= '2018-11-01'
	and d.Imported = 1
	and d.MedicalRecNo = '2091793'
	and d.HL7MessageTime is not null
	and d2.AliasName is null
	and d.Ins1Name is not null
	and d.Ins2Name is not null
group by d.AliasName
	,d.MedicalRecNo
	,cast(d.ServiceDate as date)
	,d.Ins1Name
	,d.Ins2Name
	,pay.Name
	,pays.Name