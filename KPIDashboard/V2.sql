alter procedure IHSI_PostedPeriodAverageTotalsTest @reportType char(1) as


/*
5/7/19 - Added Charges and Patients

Created 5/2/19 by Justin for Dashboard Testing


Next steps
--Provider Filtering
--Pull from top down
--Compare to today's data
--Move month end scheduling to db level
*/

/*Don't do for inactive clients*/
if exists (select CurrentPeriodNumber from IHSI_PostedPeriodDates where PeriodEnd = getdate())
	declare @postedPeriodCount int

---------------------------Testing-------------------------------
	--	,@reportType char(1) 
	
	----set @reportType = 'P'	--Use for predictions
	--set @reportType = 'V' --Use for finding processing percentages by weekday
	----set @reportType = 'A'	--Use for actuals
---------------------------------------------------------------------
	set @postedPeriodCount = 6	--Use this many posted periods in calculating the averages

	if @reportType = 'P'	---Predictions Section
		begin


		
		/*Grab posted periods*/
		
		select *
		into #tmpPostedPeriods
		from (
			select 
				top (@postedPeriodCount) ppd.CurrentPeriodNumber
			from IHSI_PostedPeriodDates ppd
			where ppd.PeriodEnd <> GETDATE()
			order by ppd.CurrentPeriodNumber desc
		)A
		
		/*Gather Payments*/
		select *
		into #tmpPayments
		from (
			select 
				pay.PostedPeriod
				,isnull(round(sum(pay.PayAmount + pay.AdjAmount), 2), 0.00) as Payments
				,s.ProviderNumber
			from #tmpPostedPeriods tpp
			left join IHSI_MGTPayments pay
				on tpp.CurrentPeriodNumber = pay.PostedPeriod
			left join Service s
				on pay.svcnumber = s.Number
			group by 
				pay.PostedPeriod
				,s.ProviderNumber
		)B

		/*Gather Charges and Patients*/
		select *
		into #tmpCharges
		from (
			select 
				s.PostedPeriod
				,isnull(round(sum(s.amount * s.units), 2), 0.00) as Charges
				,isnull(round(cast(count(distinct s.PatientNumber) as float), 2), 0.00) as Patients
				,s.ProviderNumber
			from #tmpPostedPeriods tpp
			left join Service s
				on tpp.CurrentPeriodNumber = s.PostedPeriod
				and s.Amount * s.Units > 0.01
				and ISNULL(s.TransFromSvcNum, '') = ''
			group by 
				s.PostedPeriod
				,s.ProviderNumber
		)C


		/*Calculate Averages*/
		select 
			DB_NAME() as DatabaseName
			,pro.OrganizationName
			,tp.ProviderNumber
			,round(AVG(tp.Payments), 2) as AveragePayments
			,round(AVG(tc.charges), 2) as AverageCharges
			,round(AVG(tc.Patients), 2) as AveragePatients
		from #tmpPayments tp
		left join Provider pro
			on tp.ProviderNumber = pro.Number
		left join #tmpCharges tc
			on tp.ProviderNumber = tc.ProviderNumber
		group by 
			tp.ProviderNumber
			,pro.OrganizationName
		
		drop table #tmpCharges
		drop table #tmpPayments
		drop table #tmpPostedPeriods

		end
---------------------------------------------------------------------------------------
	else if @reportType = 'V'
		begin	--Average by Weekday Section

		/*Sum payments by Weekday*/	
		select *
		into #tmpPaymentsByWeekday
		from (
			select 
				isnull(sum(pay.PayAmount + pay.AdjAmount), 0.00) as Pmts
				,DATENAME(dw, pay.PmtEntDate) as DayOWeek
				,s.ProviderNumber
			from IHSI_MGTPayments pay
			left join service s
				on pay.svcnumber = s.Number
			where pay.PmtEntDate >= DATEADD(month, -@postedPeriodCount, getdate())
			group by
				s.ProviderNumber
				,DATENAME(dw, pay.PmtEntDate)
		)A

		/*Sum charges and patients*/	
		select *
		into #tmpChargesByWeekday
		from (
			select 
				isnull(sum(s.amount * s.units), 0.00) as Charges
				,isnull(round(cast(count(distinct s.PatientNumber) as float), 2), 0.00) as Patients
				,DATENAME(dw, s.EnteredDate) as DayOWeek
				,s.ProviderNumber
			from Service s
			where s.EnteredDate >= DATEADD(MONTH, -@postedPeriodCount, getdate())
				and ISNULL(s.TransFromSvcNum, '') = ''
				and s.Amount * s.Units > 0.01
			group by
				DATENAME(dw, s.EnteredDate)
				,s.ProviderNumber
		)B

		/*Calcualte the fraction of payments made each weekday*/
		select *
		into #tmpTotalPayments
		from (
			select	tpbw.ProviderNumber
				,isnull(round(sum(tpbw.Pmts), 2), 0.00) as TotalPayments
			from #tmpPaymentsByWeekday tpbw
			group by tpbw.ProviderNumber
		)B

		/*Calculate the fraction of charges and patients entered each weekday*/
		select *
		into #tmpTotalCharges
		from (
			select tcbw.ProviderNumber
				,isnull(round(sum(tcbw.Charges), 2), 0.00) as TotalCharges
				,isnull(round(sum(tcbw.Patients), 2), 0.00) as TotalPatients
			from #tmpChargesByWeekday tcbw
			group by tcbw.ProviderNumber
		)C

		/*Serve up results*/
		select 
			DB_NAME() as DatabaseName
			,tpbw.ProviderNumber
			,tpbw.DayOWeek
			,isnull(round(tpbw.Pmts, 2), 0.00) as Pmts
			,isnull(tpbw.Pmts / ttp.TotalPayments, 0.00) as PaymentFraction
			,isnull(round(tmpChg.Charges, 2), 0.00) as Charges
			,isnull(tmpChg.Charges / totChgs.TotalCharges, 0.00) as ChargeFraction
			,isnull(round(tmpChg.Patients, 2), 0.00) as Patients
			,isnull(tmpChg.Patients / totChgs.TotalPatients, 0.00) as PatientFraction
		from #tmpPaymentsByWeekday tpbw
		left join #tmpTotalPayments ttp
			on tpbw.ProviderNumber = ttp.ProviderNumber
		left join #tmpChargesByWeekday tmpChg
			on tpbw.ProviderNumber = tmpChg.ProviderNumber
			and tpbw.DayOWeek = tmpChg.DayOWeek
		left join #tmpTotalCharges totChgs
			on tpbw.ProviderNumber = totChgs.ProviderNumber


		drop table #tmpTotalCharges
		drop table #tmpChargesByWeekday
		drop table #tmpTotalPayments
		drop table #tmpPaymentsByWeekday

		end
-----------------------------------------------------------------------------------------
	else if @reportType = 'A'

		begin	--Acutuals Section
		/*Store start and end days of the current posted period*/
		declare @from datetime = dateadd(day, 1, (select PeriodStart from IHSI_PostedPeriodDates where PeriodEnd = getdate()))
			,@to datetime = getdate()
		declare @PostedPeriod char(6) = (select max(CurrentPeriodNumber) from IHSI_PostedPeriodDates)
		declare @end datetime = (select CloseDate from CAIN.IHS_DB.dbo.IHSI_MonthEndSchedules where @@SERVERNAME + '.' + DB_NAME() = DatabaseName and PostedPeriod = @PostedPeriod) 

		--print @end

		/*Grab total payments and day counts*/
		select *
		into #tmpMain
		from (
			select 
				DB_NAME() as DatabaseName
				,ppd.CurrentPeriodNumber
				,s.ProviderNumber
				,isnull(round(sum(pay.PayAmount + pay.AdjAmount), 2), 0.00) as CurrentPayments
				,isnull(datediff(day, -7, @to)/7-datediff(day, -6, @from)/7, 0) AS MONElapsed		--Calculate the count	of each weekdays elapsed in the posted period to discount weekend production slowdown
				,isnull(datediff(day, -6, @to)/7-datediff(day, -5, @from)/7, 0) AS TUEElapsed
				,isnull(datediff(day, -5, @to)/7-datediff(day, -4, @from)/7, 0) AS WEDElapsed
				,isnull(datediff(day, -4, @to)/7-datediff(day, -3, @from)/7, 0) AS THUElapsed
				,isnull(datediff(day, -3, @to)/7-datediff(day, -2, @from)/7, 0) AS FRIElapsed
				,isnull(datediff(day, -2, @to)/7-datediff(day, -1, @from)/7, 0) AS SATElapsed
				,isnull(datediff(day, -1, @to)/7-datediff(day, 0, @from)/7, 0) AS SUNElapsed
				,isnull(datediff(day, -7, @end)/7-datediff(day, -6, @from)/7, 0) AS MONTotal		--Calculate the count	of each snull(iweekdays in the total posted period to discount weekend production slowdown
				,isnull(datediff(day, -6, @end)/7-datediff(day, -5, @from)/7, 0) AS TUETotal
				,isnull(datediff(day, -5, @end)/7-datediff(day, -4, @from)/7, 0) AS WEDTotal
				,isnull(datediff(day, -4, @end)/7-datediff(day, -3, @from)/7, 0) AS THUTotal
				,isnull(datediff(day, -3, @end)/7-datediff(day, -2, @from)/7, 0) AS FRITotal
				,isnull(datediff(day, -2, @end)/7-datediff(day, -1, @from)/7, 0) AS SATTotal
				,isnull(datediff(day, -1, @end)/7-datediff(day, 0, @from)/7, 0) AS SUNTotal
			from IHSI_PostedPeriodDates ppd
			left join IHSI_MGTPayments pay
				on ppd.CurrentPeriodNumber = pay.PostedPeriod
			left join Service s
				on pay.svcnumber = s.Number
			where ppd.PeriodEnd = getdate()
			group by
				ppd.CurrentPeriodNumber
				,s.ProviderNumber
		)A

		/*Grab total charges and patients*/
		select *
		into #tmpChg
		from (
			select 
				isnull(sum(s.Amount * s.Units), 0.00) as CurrentCharges
				,count(distinct s.PatientNumber) as CurrentPatients
				,s.ProviderNumber
			from IHSI_PostedPeriodDates ppd
			left join Service s
				on ppd.CurrentPeriodNumber = s.PostedPeriod
				and s.Amount * s.Units > 0.01
				and isnull(s.TransFromSvcNum, '') = ''
			where ppd.PeriodEnd = getdate()
			group by s.ProviderNumber
		)B

		select
			tc.CurrentCharges
			,tc.CurrentPatients
			,tm.*
		from #tmpMain tm
		left join #tmpChg tc
			on tm.ProviderNumber = tc.ProviderNumber

		drop table #tmpMain
		drop table #tmpChg

		end

	