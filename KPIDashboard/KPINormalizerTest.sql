--select *
--into #tmpWeekProviderItems
--from (
--	select kp.DayOfWeek
--		,kp.ProviderNumber
--		,kp.AvgCharge
--		,kp.AvgPts
--		,kp.AvgPayments
--	from IHSI_KPIPredictions_Test kp
--	group by kp.DayOfWeek
--		,kp.ProviderNumber
--		,kp.AvgCharge
--		,kp.AvgPts
--		,kp.AvgPayments
--)A

--select *
--into #tmpWeeklyTotals
--from (
--	select wpi.ProviderNumber
--		,sum(wpi.AvgCharge) as WeeklyChg
--		,sum(wpi.AvgPts) as WeeklyPts
--		,sum(wpi.AvgPayments) as WeeklyPmts
--	from #tmpWeekProviderItems wpi
--	group by wpi.ProviderNumber
--)B

--select *
--into #tmpPercentages
--from (
--	select wpi.DayOfWeek
--		,wpi.ProviderNumber
--		,(wpi.AvgCharge/wt.WeeklyChg)*100 as PctChg
--		,(cast(wpi.AvgPts as float)/wt.WeeklyPts)*100 as PctPts
--		,(wpi.AvgPayments/(wt.WeeklyPmts + 0.0001))*100 as PctPmts
--	from #tmpWeekProviderItems wpi
--	join #tmpWeeklyTotals wt
--		on wpi.ProviderNumber = wt.ProviderNumber
--)C

select *
into #tmpPeriodInfo
from (
	select top 6 CurrentPeriodNumber
	from IHSI_PostedPeriodDates ppd
	where ppd.PeriodEnd <> getdate()
	order by ppd.CurrentPeriodNumber desc
) TPI

select *
into #tmpPeriodAverages
from (
	select C.ProviderNumber
		,round(sum(C.AvgCharges), 0) as AvgCharges
		,round(sum(C.AvgPts), 0) as AvgPts
		,round(sum(C.AvgPmts), 0) as AvgPmts
	from (
		select s.ProviderNumber
			,sum(s.Amount * s.units)/6 as AvgCharges
			,count(distinct s.PatientNumber)/6 as AvgPts
			,'' as AvgPmts
		from #tmpPeriodInfo tpi
		join service s
			on tpi.CurrentPeriodNumber = s.PostedPeriod
			and s.Amount * s.Units > 0.01
			and isnull(s.TransToSvcNum, '') = ''
		left join Adjustment a
			on s.Number = a.ServiceNumber
			and a.AdjustmentTypeCode = '019'
		where a.ServiceNumber is null
		group by s.ProviderNumber
		
		union
		
		select s.ProviderNumber
			,'' as AvgCharges
			,'' as AvgPts
			,sum(pay.PayAmount + pay.AdjAmount)/6 as AvgPayments
		from #tmpPeriodInfo tpi
		join IHSI_MGTPayments pay
			on tpi.CurrentPeriodNumber = pay.PostedPeriod
		join Service s
			on pay.svcnumber = s.Number
		group by s.ProviderNumber
	)C
	group by C.ProviderNumber
)D
--select *
--from #tmpPercentages

--select *
--from IHSI_KPIPredictions_Test

--select *
--from #tmpPeriodAverages tpa

select *
into #tmpPredictionNormalizers
from (
	select kp.ProviderNumber
		,tpa.AvgCharges/kp.ChargesToDate as ChgRatio
		,tpa.AvgPts/cast(kp.PtsToDate as float) as PtRatio
		,case when kp.PaymentsToDate <> 0 then tpa.AvgPmts/kp.PaymentsToDate else 0 end PmtRatio
	from IHSI_KPIPredictions_Test kp
	left join IHSI_KPIPredictions_Test kp2
		on kp.ProviderNumber = kp2.ProviderNumber
		and kp2.EnteredDate > kp.EnteredDate
	join #tmpPeriodAverages tpa
		on kp.ProviderNumber = tpa.ProviderNumber
	where kp2.ProviderNumber is null
)E

--select *
--from #tmpPredictionNormalizers

--select round(kp.ChargesToDate*tpn.ChgRatio, 0)
--	,round(kp.PtsToDate*tpn.PtRatio, 0)
--	,round(kp.PaymentsToDate*tpn.PmtRatio, 0)
--	,*

update kp
set kp.ChargesToDate = round(kp.ChargesToDate*tpn.ChgRatio, 0)
	,kp.PtsToDate = round(kp.PtsToDate*tpn.PtRatio, 0)
	,kp.PaymentsToDate = round(kp.PaymentsToDate*tpn.PmtRatio, 0)

--select round(kp.ChargesToDate*tpn.ChgRatio, 0)
--	,round(kp.PtsToDate*tpn.PtRatio, 0)
--	,round(kp.PaymentsToDate*tpn.PmtRatio, 0)
--	,kp.PtsToDate
--	,tpn.PtRatio
--	,*
from IHSI_KPIPredictions_Test kp
join #tmpPredictionNormalizers tpn
	on kp.ProviderNumber = tpn.ProviderNumber
--order by EnteredDate desc

drop table #tmpPredictionNormalizers
drop table #tmpPeriodAverages
drop table #tmpPeriodInfo
--drop table #tmpPercentages
--drop table #tmpWeeklyTotals
--drop table #tmpWeekProviderItems