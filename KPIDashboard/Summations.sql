select d.Server_Name
	,d.Database_Name
	,d.Date
	,sum(cast(d.ActualCharges as int)) as ActualCharges
	,round(((sum(cast(d.ActualCharges as float)) - sum(cast(d.CTDPrediction as float))) / (sum(cast(d.CTDPrediction as float))))*100, 1) as PctOffCharges
	,sum(cast(d.ActualPts as int)) as ActualPts
	,round((sum(cast(d.ActualPts as float)) - sum(cast(d.PtsTDPrediction as float))) / (sum(cast(d.PtsTDPrediction as float)))*100, 1) as PctOffPts
	,sum(cast(d.ActualPayments as int)) as ActualPayments
	,round((sum(cast(d.ActualPayments as float)) - sum(cast(d.PayTDPrediction as float))) / (sum(cast(d.PayTDPrediction as float)))*100, 1) as PctOffPmts
	,round(sum(cast(d.ActualLevelPoints as float)) / sum(cast(d.ActualPts as float)), 1) as AverageLOS
	,round(sum(cast(d.ActualLevelPoints as float)) / sum(cast(d.ActualPts as float)) - sum(cast(d.PredictedLevelPoints as float)) / sum(cast(d.PtsTDPrediction as float)), 1) as LOSDiff
	,round(sum(cast(d.ActualCharges as float)) / sum(cast(d.ActualPts as float)), 0) as AveragePatientCharge
	,round(sum(cast(d.ActualCharges as float)) / sum(cast(d.ActualPts as float)) - sum(cast(d.CTDPrediction as float)) / sum(cast(d.PtsTDPrediction as float)), 1)  as ChargeDiff
	,sum(cast(d.CTDPrediction as int)) as CTDPrediction
	,sum(cast(d.PtsTDPrediction as int)) as PtsTDPrediction
	,sum(cast(d.PayTDPrediction as int)) as PayTDPrediction
	,d.CurrentPeriodNumber
	,d.PeriodStartDate
	,d.CloseDate
	,0 as ProviderNumber
	,d.CpnyName
	,'TOTAL DATABASE' as OrganizationName
	,sum(cast(d.ActualLevelPoints as int)) as ActualLevelPoints
	,sum(cast(d.PredictedLevelPoints as int)) as PredictedLevelPoints
from IHSI_ECPDashboard_ToBeDeleted d
where d.Database_Name in ('LA_OLOL_PEPA', 'LA_STPH_STEPG')
group by d.Server_Name
	,d.Database_Name
	,d.Date
	,d.CurrentPeriodNumber
	,d.PeriodStartDate
	,d.CloseDate
	,d.CpnyName


-------Above by database--------------


--------Below overall total----------------
select 'TOTAL' as Server_Name
	,'TOTAL' as Database_Name
	,d.Date
	,sum(cast(d.ActualCharges as int)) as ActualCharges
	,round(((sum(cast(d.ActualCharges as float)) - sum(cast(d.CTDPrediction as float))) / (sum(cast(d.CTDPrediction as float))))*100, 1) as PctOffCharges
	,sum(cast(d.ActualPts as int)) as ActualPts
	,round((sum(cast(d.ActualPts as float)) - sum(cast(d.PtsTDPrediction as float))) / (sum(cast(d.PtsTDPrediction as float)))*100, 1) as PctOffPts
	,sum(cast(d.ActualPayments as int)) as ActualPayments
	,round((sum(cast(d.ActualPayments as float)) - sum(cast(d.PayTDPrediction as float))) / (sum(cast(d.PayTDPrediction as float)))*100, 1) as PctOffPmts
	,round(sum(cast(d.ActualLevelPoints as float)) / sum(cast(d.ActualPts as float)), 1) as AverageLOS
	,round(sum(cast(d.ActualLevelPoints as float)) / sum(cast(d.ActualPts as float)) - sum(cast(d.PredictedLevelPoints as float)) / sum(cast(d.PtsTDPrediction as float)), 1) as LOSDiff
	,round(sum(cast(d.ActualCharges as float)) / sum(cast(d.ActualPts as float)), 0) as AveragePatientCharge
	,round(sum(cast(d.ActualCharges as float)) / sum(cast(d.ActualPts as float)) - sum(cast(d.CTDPrediction as float)) / sum(cast(d.PtsTDPrediction as float)), 1)  as ChargeDiff
	,sum(cast(d.CTDPrediction as int)) as CTDPrediction
	,sum(cast(d.PtsTDPrediction as int)) as PtsTDPrediction
	,sum(cast(d.PayTDPrediction as int)) as PayTDPrediction
	,d.CurrentPeriodNumber
	,d.PeriodStartDate
	,d.CloseDate
	,0 as ProviderNumber
	,'TOTAL' as CpnyName
	,'TOTAL' as OrganizationName
	,sum(cast(d.ActualLevelPoints as int)) as ActualLevelPoints
	,sum(cast(d.PredictedLevelPoints as int)) as PredictedLevelPoints
from IHSI_ECPDashboard_ToBeDeleted d
where d.date = cast(getdate() as date)
group by d.Date
	,d.CurrentPeriodNumber
	,d.PeriodStartDate
	,d.CloseDate



---------Above overall total-----------

--------Below PEPA Main------------
declare @totalPtsToDatePEPABR float = (select sum(cast(ed.PtsTDPrediction as float)) from IHSI_ECPDashboard_ToBeDeleted ed where ed.Database_Name = 'LA_OLOL_PEPA' and ed.ProviderNumber not in (6, 8))

declare @LOSPredictionPEPABR float = (select round(sum((PtsTDPrediction/@totalPtsToDatePEPABR) * ed.AverageLOS), 1)
from IHSI_ECPDashboard_ToBeDeleted ed
where ed.Database_Name = 'LA_OLOL_PEPA'
	and ed.ProviderNumber not in (6, 8))

select d.Server_Name
	,d.Database_Name
	,d.Date
	,sum(cast(d.ActualCharges as int)) as ActualCharges
	,round(((sum(cast(d.ActualCharges as float)) - sum(cast(d.CTDPrediction as float))) / (sum(cast(d.CTDPrediction as float))))*100, 1) as PctOffCharges
	,sum(cast(d.ActualPts as int)) as ActualPts
	,round((sum(cast(d.ActualPts as float)) - sum(cast(d.PtsTDPrediction as float))) / (sum(cast(d.PtsTDPrediction as float)))*100, 1) as PctOffPts
	,sum(cast(d.ActualPayments as int)) as ActualPayments
	,round((sum(cast(d.ActualPayments as float)) - sum(cast(d.PayTDPrediction as float))) / (sum(cast(d.PayTDPrediction as float)))*100, 1) as PctOffPmts
	,round(sum(cast(d.ActualLevelPoints as float)) / sum(cast(d.ActualPts as float)), 1) as AverageLOS
	,round(sum(cast(d.ActualLevelPoints as float)) / sum(cast(d.ActualPts as float)), 1) - @LOSPredictionPEPABR as LOSDiff
	--sum(cast(d.PredictedLevelPoints as float)) / sum(cast(d.PtsTDPrediction as float)), 1) as LOSDiff
	,round(sum(cast(d.ActualCharges as float)) / sum(cast(d.ActualPts as float)), 0) as AveragePatientCharge
	,round(sum(cast(d.ActualCharges as float)) / sum(cast(d.ActualPts as float)) - sum(cast(d.CTDPrediction as float)) / sum(cast(d.PtsTDPrediction as float)), 1)  as ChargeDiff
	,sum(cast(d.CTDPrediction as int)) as CTDPrediction
	,sum(cast(d.PtsTDPrediction as int)) as PtsTDPrediction
	,sum(cast(d.PayTDPrediction as int)) as PayTDPrediction
	,d.CurrentPeriodNumber
	,d.PeriodStartDate
	,d.CloseDate
	,0 as ProviderNumber
	,d.CpnyName
	,'PROF EMERG PHYS ASSOC - BR' as OrganizationName
	,sum(cast(d.ActualLevelPoints as int)) as ActualLevelPoints
	,sum(cast(d.PredictedLevelPoints as int)) as PredictedLevelPoints
from IHSI_ECPDashboard_ToBeDeleted d
where d.CpnyName = 'PEPA AT OLOL'
	and d.ProviderNumber not in (6, 8)
group by d.Server_Name
	,d.Database_Name
	,d.Date
	,d.CurrentPeriodNumber
	,d.PeriodStartDate
	,d.CloseDate
	,d.CpnyName