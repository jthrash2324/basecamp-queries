if (exists (select * from IHSI_KPIPredictions where EnteredDate = cast(getdate() as date))
	and (select count(*) from IHSI_CodingEntries ce) > 10)
begin

select *
into #tmpServiceLevelDetail
from (
	select s.PatientNumber
		,sum(s.Amount * s.Units) as Charges
		,cast(isnull(right(max(s2.ServiceItemCode), 1), '6') as float) as LOSCode
		,s.ProviderNumber
	from service s
	join IHSI_PostedPeriodDates ppd
		on s.PostedPeriod = ppd.CurrentPeriodNumber
	left join Service s2
		on s.PatientNumber = s2.PatientNumber
		and s2.PostedPeriod = ppd.CurrentPeriodNumber
		and isnull(s2.TransFromSvcNum, '') = ''
		and s2.ServiceItemCode in ('99281', '99282', '99283', '99284', '99285',
									'99201', '99202', '99203', '99204', '99205',
									'99211', '99212', '99213', '99214', '99215')
	left join Adjustment a
		on s.Number = a.ServiceNumber
		and a.AdjustmentTypeCode = '019'
	left join Adjustment a2
		on s2.Number = a2.ServiceNumber
		and a2.AdjustmentTypeCode = '019'
	where ppd.PeriodEnd = getdate()
		and isnull(s.TransFromSvcNum, '') = ''
		and s.Amount * s.Units > 0.01
		and a.ServiceNumber is null
		and a2.ServiceNumber is null
	group by s.PatientNumber
		,s.ProviderNumber
)SLD

--select * from #tmpServiceLevelDetail

if (select count(*) from #tmpServiceLevelDetail) = 0
begin 
	insert into #tmpServiceLevelDetail  values 
		(
			'000000001'
			,0
			,0
			,'0000001'
		)
end

select sum(X.ActualPts) as ActualPts
	,sum(X.ActualCharges) as ActualCharges
	,sum(X.ActualPayments) as ActualPayments
	,max(X.AverageLOS) as AverageLOS
	,max(X.AveragePatientCharge) as AveragePatientCharge
	,cast(GETDATE() as date) as EnteredDate
	,X.ProviderNumber
	,sum(X.LevelPoints) as LevelPoints
into #tmpActuals
from (
	select count(distinct sld.PatientNumber) as ActualPts
		,sum(sld.Charges) as ActualCharges
		,'' as ActualPayments
		,sum(sld.LOSCode) / count(distinct sld.PatientNumber) as AverageLOS
		,round(sum(sld.Charges) / count(distinct sld.PatientNumber), 0) as AveragePatientCharge
		,sld.ProviderNumber
		,sum(sld.LOSCode) as LevelPoints
	from #tmpServiceLevelDetail sld
	group by sld.ProviderNumber
	
	union
	
	select '' as ActualPts
		,'' as ActualCharges
		,round(isnull(sum(p.PayAmount + p.AdjAmount), 0), 0) as ActualPayments
		,'' as AverageLOS
		,'' as AveragePatientCharge
		,s.ProviderNumber
		,'' as LevelPoints
	from IHSI_MGTPayments p
	join IHSI_PostedPeriodDates ppd
		on p.PostedPeriod = ppd.CurrentPeriodNumber
	left join service s
		on p.svcnumber = s.Number
	where ppd.PeriodEnd = getdate()
	group by s.ProviderNumber
)X
group by X.ProviderNumber

--select * from #tmpActuals

select distinct cast(GETDATE() as date) as Date
	,ta.ActualCharges
	----,kp.ChargesToDate
	--,round((ta.ActualCharges - kp.ChargesToDate) / (kp.ChargesToDate / (select count(distinct EnteredDate) from IHSI_KPIPredictions where EnteredDate <= GETDATE())), 1) as DaysOffCharges
	,round(((ta.ActualCharges - kp.ChargesToDate)/(kp.ChargesToDate + 0.01)) * 100, 1) as PctOffCharges
	,ta.ActualPts
	--,round((cast(ta.ActualPts - kp.PtsToDate as float)) / (kp.PtsToDate / (select count(distinct EnteredDate) from IHSI_KPIPredictions where EnteredDate <= GETDATE())), 1) as DaysOffPts
	, case when cast(kp.PtsToDate as float) = 0 then 0.0 else
	round((cast((ta.ActualPts - kp.PtsToDate) as float)/cast(kp.PtsToDate as float) * 100), 1) end as PctOffPts
 	,ta.ActualPayments
	--,round((cast(ta.ActualPayments - kp.PaymentsToDate as float)) / (kp.PaymentsToDate / (select count(distinct EnteredDate) from IHSI_KPIPredictions where EnteredDate <= GETDATE())), 1) as DaysOffPmts
	,case when kp.PaymentsToDate = 0 then 0.0
		else round(((ta.ActualPayments - kp.PaymentsToDate)/(kp.PaymentsToDate + 0.01))*100, 1) end as PctOffPmts
	,round(ta.AverageLOS, 1) as AverageLOS
	,round(ta.AverageLOS - kp.LOS, 1) as LOSDiff
 	,ta.AveragePatientCharge		
	,ta.AveragePatientCharge - kp.APC as ChargeDiff
	,kp.ChargesToDate as CTDPrediction
	,kp.PtsToDate as PtsTDPrediction
	,kp.PaymentsToDate as PayTDPrediction
	,ppd.CurrentPeriodNumber
	,cast(ppd.PeriodStart as date) as PeriodStartDate
	,mes.CloseDate
	,kp.ProviderNumber
	,mes.CpnyName
	,p.OrganizationName
	,ta.LevelPoints as ActualLevelPoints
	,kp.PredictedLevelPoints
from #tmpActuals ta
left join IHSI_KPIPredictions kp
	on ta.EnteredDate = kp.EnteredDate
	and ta.ProviderNumber = kp.ProviderNumber
left join IHSI_PostedPeriodDates ppd
	on ta.EnteredDate >= ppd.PeriodStart
	and ta.EnteredDate <= ppd.PeriodEnd
left join CAIN.IHS_DB.dbo.IHSI_MonthEndSchedules mes
	on @@SERVERNAME + '.' + DB_NAME() = mes.DatabaseName
	and ppd.CurrentPeriodNumber = mes.PostedPeriod
join Provider p
	on kp.ProviderNumber = p.Number
--group by ta.ActualCharges
--	,kp.ChargesToDate
--	,ta.ActualPts
--	,kp.PtsToDate
--	,ta.ActualPayments
--	,kp.PaymentsToDate
--	,ta.AverageLOS
--	,kp.LOS
--	,ta.AveragePatientCharge
--	,kp.APC
--	,kp.ChargesToDate
--	,kp.PtsToDate
--	,kp.PaymentsToDate
--	,ppd.CurrentPeriodNumber
--	,cast(ppd.PeriodStart as date)
--	,mes.CloseDate
--	,kp.ProviderNumber
	




drop table #tmpServiceLevelDetail
drop table #tmpActuals

end