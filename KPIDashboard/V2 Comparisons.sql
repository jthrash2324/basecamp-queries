/*I created this table on CAIN.IHS_DB to store the results for now
create table IHSI_KPIPredictions_Test_V2 (
	DataBaseName nvarchar(max) not null
	,OrganizationName nvarchar(max) not null
	,ProviderNumber char(7) not null
	,PaymentTarget float not null
	,CurrentPayments float not null
	,Difference float not null
	,PercentDifference float not null
	,CurrentPeriodNumber char(6) not null
)*/


--exec IHSI_PostedPeriodAverageTotalsTest @reportType = 'P'
exec IHSI_PostedPeriodAverageTotalsTest @reportType = 'V'
--exec IHSI_PostedPeriodAverageTotalsTest @reportType = 'A'

/*Grab the PostedPeriodAverages*/
create table #tmpPPAverages (
	DataBaseName nvarchar(max) not null
	,OrganizationName nvarchar(max) 
	,ProviderNumber char(7)
	,AveragePayments float not null
	,AverageCharges float not null
	,AveragePatients float not null
)

insert into #tmpPPAverages
exec IHSI_PostedPeriodAverageTotalsTest @reportType = 'P'

/*Grab the weekday fractions*/
create table #tmpWeekDayAverages (
	DataBaseName nvarchar(max) not null
	,ProviderNumber char(7)
	,DayOWeek nvarchar(max) not null
	,Pmts float not null
	,PaymentFraction float not null
	,Charges float not null
	,ChargeFraction float not null
	,Patients float not null
	,PatientFraction float not null
)

insert into #tmpWeekDayAverages
exec IHSI_PostedPeriodAverageTotalsTest @reportType = 'V'


/*Grab the current data*/
create table #tmpCurrentData (
	CurrentCharges float not null
	,CurrentPatients float not null
	,DataBaseName nvarchar(max) not null
	,CurrentPeriodNumber char(6) not null
	,ProviderNumber char(7)
	,CurrentPayments float not null
	,MONElapsed	int not null	
	,TUEElapsed	int not null
	,WEDElapsed	int not null
	,THUElapsed	int not null
	,FRIElapsed	int not null
	,SATElapsed	int not null
	,SUNElapsed	int not null
	,MONTotal int not null		
	,TUETotal int not null
	,WEDTotal int not null
	,THUTotal int not null
	,FRITotal int not null
	,SATTotal int not null
	,SUNTotal int not null
)

insert into #tmpCurrentData
exec IHSI_PostedPeriodAverageTotalsTest @reportType = 'A'


--------------------/*Calculate distance from projections*/-----------------------

/*Calculate the total predicted payments/weekday/pp*/
--This takes the average total for the posted period and breaks it down into 7 parts; how much of the total is each day of the week responsible for
select *		
into #tmpPredByWeekDay
from (
	select tpp.DataBaseName
		,tpp.OrganizationName
		,tpp.ProviderNumber
		,twa.DayOWeek
		,round(tpp.AveragePayments * twa.PaymentFraction, 2) as TotalPaymentsByWeekDayPred
	from #tmpPPAverages tpp
	left join #tmpWeekDayAverages twa
		on tpp.DataBaseName = twa.DataBaseName
		and tpp.ProviderNumber = twa.ProviderNumber
)A

--After this (getting the fractons of each weekday passed)you have everything you need to sum up the prediction and compare to the actuals
select *
into #tmpPrePrediction
from (
	select twd.*
		,case
			when twd.DayOWeek = 'Sunday' then cast(tcd.SUNElapsed as float) / cast(tcd.SUNTotal as float)
			when twd.DayOWeek = 'Monday' then cast(tcd.MONElapsed as float) / cast(tcd.MONTotal as float)
			when twd.DayOWeek = 'Tuesday' then cast(tcd.TUEElapsed as float) / cast(tcd.TUETotal as float)
			when twd.DayOWeek = 'Wednesday' then cast(tcd.WEDElapsed as float) / cast(tcd.WEDTotal as float)
			when twd.DayOWeek = 'Thursday' then cast(tcd.THUElapsed as float) / cast(tcd.THUTotal as float)
			when twd.DayOWeek = 'Friday' then cast(tcd.FRIElapsed as float) / cast(tcd.FRITotal as float)
			when twd.DayOWeek = 'Saturday' then cast(tcd.SATElapsed as float) / cast(tcd.SATTotal as float)
		end as 'FractionPassed'
	from #tmpPredByWeekDay twd
	left join #tmpCurrentData tcd
		on twd.DataBaseName = tcd.DataBaseName
		and twd.ProviderNumber = tcd.ProviderNumber
)B

insert into CAIN.IHS_DB.dbo.IHSI_KPIPredictions_Test_V2
select *
--into #tmpRaw
from
(
	select 
		tpp.DataBaseName
		,tpp.OrganizationName
		,tpp.ProviderNumber
		--,*
		,sum(tpp.TotalPaymentsByWeekDayPred * tpp.FractionPassed) as PaymentTarget
		,tcd.CurrentPayments
		,tcd.CurrentPayments - sum(tpp.TotalPaymentsByWeekDayPred * tpp.FractionPassed) as Difference
		,((tcd.CurrentPayments - sum(tpp.TotalPaymentsByWeekDayPred * tpp.FractionPassed)) / sum	(tpp.TotalPaymentsByWeekDayPred * tpp.FractionPassed)) * 100 as PercentDifference
		--,1.0 - (tcd.CurrentPayments / sum(tpp.TotalPaymentsByWeekDayPred * tpp.FractionPassed)) as FractionMissing
		,tcd.CurrentPeriodNumber
	from 
	#tmpPrePrediction tpp
	left join #tmpCurrentData tcd
		on tpp.DataBaseName = tcd.DataBaseName
		and tpp.ProviderNumber = tcd.ProviderNumber
	where tcd.databasename is not null
	group by tpp.DataBaseName
		,tpp.OrganizationName
		,tpp.ProviderNumber
		,tcd.CurrentPayments
		,tcd.CurrentPeriodNumber
)C

/*Format results for KF*/
--select
--	tr.*
--	,cast(round(tr.CurrentPayments, 2) as nvarchar(50)) + ' (' + cast(round(tr.PercentDifference, 1) as nvarchar(50)) + ')' as Payments
--from #tmpRaw tr

----select * from #tmpPPAverages
--select * from #tmpWeekDayAverages
--select * from #tmpCurrentData


--drop table #tmpRaw
drop table #tmpPrePrediction
drop table #tmpPredByWeekDay
drop table #tmpCurrentData
drop table #tmpWeekDayAverages
drop table #tmpPPAverages