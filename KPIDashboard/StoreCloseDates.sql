insert into IHSI_MonthEndSchedules (DatabaseName
	,CpnyID
	,CpnyName
	,PostedPeriod)
select DatabaseName
	,CpnyID
	,CpnyName
	,'201905'
from IHSI_ActiveClients

update IHSI_MonthEndSchedules
set CloseDate = '2019-06-01'
where cpnyid in (
'032'
,'368'
,'370'
,'393'
)
and postedperiod = '201905'


update IHSI_MonthEndSchedules
set CloseDate = '2019-06-05'
where cpnyid in (
'206'
,'281'
,'394'
,'280'
,'286'
,'289'
,'377'
,'381'
,'378'
,'360'
,'382'
,'383'
,'384'
,'385'
,'387'
)
and postedperiod = '201905'


update IHSI_MonthEndSchedules
set CloseDate = '2019-06-06'
where cpnyid in (
'388'
,'389'
,'176'
,'GRM'
,'392'
,'365'
,'163'
,'386'
,'391'
,'366'
,'146'
,'120'
,'180'
,'181'
,'148'
)
and postedperiod = '201905'


update IHSI_MonthEndSchedules
set CloseDate = '2019-06-07'
where cpnyid in (
'371'
,'395'
,'374'
,'372'
,'375'
,'376'
,'373'
,'190'
,'397'
)
and postedperiod = '201905'

delete from IHSI_MonthEndSchedules
where CloseDate is null

select * from IHSI_MonthEndSchedules