--create table IHSI_KPIPredictions_Test (
--	EnteredDate date not null
--	,DayOfWeek nvarchar(9) not null
--	,AvgCharge float not null
--	,AvgPts int not null
--	,AvgPayments float not null
--	,ChargesToDate float not null
--	,PtsToDate int not null
--	,PaymentsToDate int not null
--  ,ProviderNumber char(7) null
--	,LOS float null
--	,APC int null
--	,PredictedLevelPoints int null
--)

--drop table IHSI_KPIPredictions_Test

if exists (select top 1 * from service s where s.FromDate >= DATEADD(MONTH, -1, GETDATE()))
begin

select *
into #tmpPeriodInfo
from (
	select top 6 *
		,DATEDIFF(day, ppd.PeriodStart, ppd.PeriodEnd) as DaysInPeriod
	from IHSI_PostedPeriodDates ppd
	where ppd.PeriodEnd <> getdate()
	order by ppd.CurrentPeriodNumber desc
) TPI

declare @startdate date
declare @enddate date

set @startdate = (select cast(min(periodstart) as date) from #tmpPeriodInfo)
set @enddate = (select cast(max(periodend) as date) from #tmpPeriodInfo)

select *
into #tmpDateRange
from (
	SELECT  DATEADD(DAY, nbr - 1, @StartDate) as EnteredDate
	FROM    ( SELECT    ROW_NUMBER() OVER ( ORDER BY c.object_id ) AS Nbr
	          FROM      sys.columns c
	        ) nbrs
	WHERE   nbr - 1 <= DATEDIFF(DAY, @StartDate, @EndDate)
)TDR


select *
into #tmpDailyData
from (
	select distinct tdr.EnteredDate
		,count(distinct s.PatientNumber) as Pts
		,isnull(sum(s.Amount * s.Units), 0) as Charges
		,case when tdr.EnteredDate = cast(tpi.PeriodStart as date) then 1 else 0 end as IsPeriodStartDate
		,case when tdr.EnteredDate = cast(tpi.PeriodEnd as date) then 1 else 0 end as IsEndDate
		,DATENAME(dw, tdr.EnteredDate) as DayOfWeek
		,tpi.CurrentPeriodNumber
		,s.ProviderNumber
	from #tmpDateRange tdr
	join #tmpPeriodInfo tpi
		on tdr.EnteredDate >= cast(tpi.PeriodStart as date)
		and tdr.EnteredDate <= cast(tpi.PeriodEnd as date)
	left join Service s
		on tdr.EnteredDate = cast(s.EnteredDate as date)
		and isnull(s.TransFromSvcNum, '') = ''
		and s.Amount * s.Units > 0.01
		and tpi.CurrentPeriodNumber = s.PostedPeriod
	left join Service s2
		on s.PatientNumber = s2.PatientNumber
		and s2.Amount * s2.Units > 0.01
		and s2.PostedPeriod < s.PostedPeriod
	left join Adjustment a
		on s.Number = a.ServiceNumber
		and a.AdjustmentTypeCode = '019'
	where a.ServiceNumber is null
		and s2.PatientNumber is null
	group by tdr.EnteredDate
		,tpi.CurrentPeriodNumber
		,tpi.PeriodStart
		,tpi.PeriodEnd
		,tpi.DaysInPeriod
		,s.ProviderNumber
)TDD

--select * from #tmpDailyData

select *
into #tmpPaymentData
from (
	select tdd.EnteredDate
		,isnull(p.PostedPeriod, tdd.CurrentPeriodNumber) as PostedPeriod
		,round(isnull(sum(p.PayAmount + p.AdjAmount), 0), 2) as Payments
		,tdd.ProviderNumber
	from #tmpDailyData tdd
	left join IHSI_MGTPayments p
		on tdd.EnteredDate = cast(p.PmtEntDate as date)
		and tdd.CurrentPeriodNumber = p.PostedPeriod
	join Service s
		on p.svcnumber = s.Number
		and tdd.ProviderNumber = s.ProviderNumber
	group by tdd.EnteredDate
		,p.PostedPeriod
		,tdd.CurrentPeriodNumber
		,tdd.ProviderNumber
) TPD

--select * from #tmpPaymentData

select *
into #tmpAverages
from (
	select tdd.DayOfWeek
		,sum(tdd.Charges) as Charges
		,sum(tdd.Pts) as Pts
		,count(distinct tdd.EnteredDate) as Counts
		,round(sum(tdd.Charges)/count(distinct tdd.EnteredDate), 2) as AvgCharge
		,round(sum(tdd.Pts)/count(distinct tdd.EnteredDate), 2) as AvgPts
		,isnull(round(sum(tpd.Payments)/count(distinct tdd.EnteredDate), 2), 0.00) as AvgPayments
		,isnull(tdd.ProviderNumber, '0000001') as ProviderNumber
	from #tmpDailyData tdd
	left join #tmpPaymentData tpd
		on tdd.EnteredDate = tpd.EnteredDate
		and tdd.CurrentPeriodNumber = tpd.PostedPeriod
		and tdd.ProviderNumber = tpd.ProviderNumber
	where tdd.IsPeriodStartDate = 0
	group by tdd.DayOfWeek
		,tdd.ProviderNumber
) TA

--select * from #tmpAverages --where ProviderNumber = '0000001'

declare @CurrStartDate date = (select dateadd(day, 1, cast(max(PeriodEnd) as date)) from #tmpPeriodInfo)
declare @CurrEndDate date = (select max(CloseDate) from CAIN.IHS_DB.dbo.IHSI_MonthEndSchedules where @@SERVERNAME + '.' + DB_NAME() = DatabaseName)

select *
into #tmpCurrDateRange
from (
	SELECT  DATEADD(DAY, nbr - 1, @CurrStartDate) as EnteredDate
	FROM    ( SELECT    ROW_NUMBER() OVER ( ORDER BY c.object_id ) AS Nbr
	          FROM      sys.columns c
	        ) nbrs
	WHERE   nbr - 1 <= DATEDIFF(DAY, @CurrStartDate, @CurrEndDate)
)TCDR

select *
into #tmpPrediction
from (
	select tcdr.EnteredDate
		,DATENAME(dw, tcdr.EnteredDate) as DayOfWeek
		,ta.AvgCharge
		,ta.AvgPts
		,ta.AvgPayments
		,ta.ProviderNumber
	from #tmpCurrDateRange tcdr
	left join #tmpAverages ta
		on DATENAME(dw, tcdr.EnteredDate) = ta.DayOfWeek
)TP

--select * from #tmpPrediction

select *
into #tmpPredictionsToDate
from (
	select tp.EnteredDate
		,tp.DayOfWeek
		,tp.AvgCharge
		,tp.AvgPts
		,tp.AvgPayments
		,isnull(round(sum(tp2.AvgCharge), 0), 0) as ChargesToDate
		,isnull(sum(tp2.AvgPts), 0) as PtsToDate
		,isnull(round(sum(tp2.AvgPayments), 0), 0) as PaymentsToDate
		,isnull(tp.ProviderNumber, '0000001') as ProviderNumber
	from #tmpPrediction tp
	left join #tmpPrediction tp2
		on tp.EnteredDate >= tp2.EnteredDate
		and tp.ProviderNumber = tp2.ProviderNumber
	group by tp.EnteredDate
		,tp.DayOfWeek
		,tp.AvgCharge
		,tp.AvgPts
		,tp.AvgPayments
		,tp.ProviderNumber
)TPTD

--select * from #tmpPredictionsToDate
select *
into #tmpPatientAveragesCharges
from (
	select s.PatientNumber
		,sum(s.Amount * s.Units) as Charges
		--,0.0 as LOS
		,s.ProviderNumber
	from Service s
	join #tmpPeriodInfo tpi
		on s.PostedPeriod = tpi.CurrentPeriodNumber
	left join Adjustment a
		on s.Number = a.ServiceNumber
		and a.AdjustmentTypeCode = '019'
	left join Service s2
		on s.PatientNumber = s2.PatientNumber
		and s2.Amount * s2.Units > 0.01
		and s2.PostedPeriod < s.PostedPeriod
	where s.Amount * s.Units > 0.01
		and ISNULL(s.TransFromSvcNum, '') = ''
		and a.ServiceNumber is null
		and s2.PatientNumber is null
	group by s.PatientNumber
		,s.ProviderNumber

	--union


)TPAC

--select count(*) from #tmpPatientAveragesCharges

select *
into #tmpPatientAverages
from (
	select tpa.PatientNumber
		,tpa.Charges
		--,tpa.LOS
		,tpa.ProviderNumber
		--,max(s.ServiceItemCode) as MaxLOSCode 
		,case
			when left(max(s.ServiceItemCode), 4) = '9929' then 6.0
			else cast(isnull(right(max(s.ServiceItemCode), 1), '0') as float)
		end as LOS
	--update tpa
	--set LOS = case
	--			when left(max(s.ServiceItemCode), 4) = '9929' then 6.0
	--			else cast(isnull(right(max(s.ServiceItemCode), 1), '0') as float)
	--			end
	from #tmpPatientAveragesCharges tpa
	left join service s
		on tpa.PatientNumber = s.PatientNumber
		and isnull(s.TransFromSvcNum, '') = ''
		and s.ServiceItemCode in ('99281', '99282', '99283', '99284', '99285', '99291', '99292',
									'99201', '99202', '99203', '99204', '99205',
									'99211', '99212', '99213', '99214', '99215')
	left join Adjustment a
		on s.Number = a.ServiceNumber
		and a.AdjustmentTypeCode = '019'
	where a.ServiceNumber is null
	group by tpa.PatientNumber
		,tpa.Charges
		--,tpa.LOS
		,tpa.ProviderNumber
)TPA

--select * from #tmpPatientAverages

--select count(*) from #tmpPatientAverages

select *
into #tmpLOS
from (
	select (sum(tpa.LOS) / count(distinct tpa.PatientNumber)) as AvgLOS
		,round(sum(tpa.Charges) / count(distinct tpa.PatientNumber), 0) as AvgPtCharge
		,tpa.ProviderNumber 
		--,sum(tpa.LOS) as PredictedLevelPoints
	from #tmpPatientAverages tpa 
	group by tpa.ProviderNumber
) TLOS

--select * from #tmpLOS

--insert into IHSI_KPIPredictions_Test
select tptd.EnteredDate
	,tptd.DayOfWeek
	,sum(tptd.AvgCharge) as AvgCharge
	,sum(tptd.AvgPts) as AvgPts
	,sum(tptd.AvgPayments) as AvgPayments
	,max(tptd.ChargesToDate) as ChargesToDate
	,max(tptd.PtsToDate) as PtsToDate
	,max(tptd.PaymentsToDate) as PaymentsToDate
	,tptd.ProviderNumber
	,max(tlos.AvgLOS) as AvgLOS
	,max(tlos.AvgPtCharge) as AvgPtCharge
	--,sum(round(tptd.AvgPts*tlos.AvgLos,0)) as PredictedLevelPoints
	,round(max(tptd.PtsToDate)*max(tlos.AvgLOS), 0) as PredictedLevelPoints
	------,max(tlos.PredictedLevelPoints) as PredictedLevelPoints
 --------select tptd.*
	--------,tlos.AvgLOS
	--------,tlos.AvgPtCharge
	------------,@APC
	------------,@LOS
from #tmpPredictionsToDate tptd
join #tmpLOS tlos
	on tptd.ProviderNumber = tlos.ProviderNumber
group by tptd.EnteredDate
	,tptd.DayOfWeek
	,tptd.ProviderNumber
--order by tptd.EnteredDate desc
--	,tptd.ProviderNumber asc

drop table #tmpLOS
drop table #tmpPatientAverages
drop table #tmpPatientAveragesCharges
drop table #tmpPredictionsToDate
drop table #tmpPrediction
drop table #tmpCurrDateRange
drop table #tmpAverages
drop table #tmpPaymentData
drop table #tmpDailyData
drop table #tmpDateRange
drop table #tmpPeriodInfo


----------------------------------------------Now Normalize the Predictions (previously in different query)-----------------------------



end


