/*
Next steps:
--Add in total days in posted period to calculate the %off; alternatively just use an average of say 30
--Use this to calculate the changes
--Throw query into Klipfolio
--Setup drop down dashboard with db and provider
*/


select *
from PostedPeriotAverageTotalsTest ppat
left join DailyAveragesTest dat
	on ppat.ProviderNumber = dat.ProviderNumber
	and ppat.Database_Name = dat.Database_Name
where ppat.Database_Name = 'IL_SCH_EMS'	--Testing

select *
from DailyAveragesTest dat

select *
from KPIActualsTest kat

--drop table DailyAveragesTest

