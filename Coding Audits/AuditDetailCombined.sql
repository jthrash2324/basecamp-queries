
DECLARE @SequenceSeed int
	,@AuditCount int

IF EXISTS (select * from SIVSystem.dbo.Company where DatabaseName = db_name() and User1 like 'E%')  --E = ED client type, otherwise run the Clinical code.
begin


----Query to Identify Accounts for Audit

---Note:  The end DOS parameter should be set one day ahead of the range you're trying to capture since it's a "less than" sign and not "less than or equal to" 




SET @SequenceSeed = '0000' --Increment for each new batch

SET @AuditCount = (select cast(count(distinct ce.Staging_ID) * .02 as int)
from IHSI_CodingEntries ce
join service s
	on ce.ServiceNumber = s.Number
where ce.CodedDate >= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0)
	and ce.CodedDate < DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1)
	and s.Amount * s.Units > 0.01)
;
WITH A (PatientNumber, HID, DOS, CoderNumber, CPT, PayorNumber, PayorTypeCode, PayorTypeDescription, PayorName, Billing, BillingClinician, Servicing, ServicingClinician, ProviderNumber, PatientDOB, Gender, MRN)
AS
(
select top (@AuditCount) --to select the quantity desired, change this number.
	   s.PatientNumber
      , p.aliasname as HID
      , s.fromdate as DOS 
      , co.codedbyid as CoderNumber
      , s.serviceitemcode as CPT
	  , s.PayorNumber
	  , pay.TypeCode as PayorTypeCode
	  , pt.Description as PayorTypeDescription
	  , pay.Name as PayorName
               , s.performedbyid as Billing
               , c.Name as BillingClinician
	  , a.AttendingClinicianID as Servicing 
	  , c2.Name as ServicingClinician
	   --,s.comment as Comment
	  --, co.codeddate
	  , s.ProviderNumber 
	  , p.Birthdate as PatientDOB
	,p.Sex as Gender
	,p.Name as Patient
--into #tmpAudit
from dbo.service as s
      LEFT JOIN dbo.ihsi_codingentries as co ON s.number = co.servicenumber
      LEFT JOIN dbo.patient as p on p.number = co.patientnumber
      LEFT JOIN dbo.Payor as pay on pay.number = s.payornumber
      LEFT JOIN dbo.IHSI_PayorType as PT on pt.code = pay.typecode
      LEFT JOIN dbo.Admission a on a.number = s.admissionnumber
      LEFT JOIN dbo.clinician c on c.number = s.performedbyid  --Billing Clinician
      LEFT JOIN dbo.clinician c2 on c2.number = a.AttendingClinicianID  --Servicing Clinician
where s.patientnumber = p.number 
     --and co.codedbyid like '%1051'  --Coder number.
     --and s.performedbyid = '0001040'  --Billing
     --and a.attendingclinicianid  = '0000028'  --Servicing
      --and s.fromdate >= '2014-09-01'and s.fromdate < '2014-10-23'  --Date of service range.
      --and s.serviceitemcode in ('99281','99282','99283','99284','99285','99291') -- CPT selection.
      --and s.serviceitemcode >= '99406' and s.serviceitemcode <='99406' --CPT selection range
     --and s.serviceitemcode = 'TRANS' --specific CPT
      --and co.codeddate between '2014-03-04 00:00:00' and '2014-03-06 00:00:00' --Coded date
	  --and s.payornumber in ('0000001','0000003') -- Specific payor selection
	 --and s.payornumber in (select Number from dbo.Payor where typecode in ('B','C','D','F','G','H','J','K')) -- payor type selection.
	--and s.ProviderNumber = '0000002'  --To isolate a specific provider --added on 9/17/12 per SR 8115
	      and co.CodedDate >= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) --First day of previous month
	  and co.CodedDate < DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) --Last Day of previous month  
	and s.Amount * s.Units > 0
order by newid() --to randomize results.
)


select @SequenceSeed + DENSE_RANK() OVER (order by x.PatientNumber) as No
	, '' as Auditor
	--, '' as PeriodPlaceholder
	, p.OrganizationName as Facility
	, x.PatientNumber as Account
      , x.HID
      , x.DOS 
      --, x.CoderNumber 
      , st.Name as Coder
      , co.ServiceItemCode as CPT
      , co.Modifier1 as Modifier
	  ,'' as [Auditor E&M Review]
	  ,'' as [Auditor CPT, Modifiers MIPS]
	  ,'' as [Auditor Comments]
	  ,'' as [Points Possible]
	  ,'' as [Actual Points]
      , co.ICDTen1 as DX1
      , co.ICDTen2 as DX2
      , co.ICDTen3 as DX3
      , co.ICDTen4 as DX4
	  	  ,'' as [Auditor DX]
	  ,'' as [Auditor Comments3]
	  ,'' as [Points Possible4]
	  ,'' as [Actual Points5]
	  --, x.PayorNumber
	  --, x.PayorTypeCode
	  , x.PayorTypeDescription
	  , x.PayorName
	  --, x.Billing
               , x.BillingClinician as [Billing Provider]
		,'' as [BILLING Coded Correctly?]
		,'' as [Points Possible]
		,'' as [Actual Points2]
	  --, x.Servicing 
               , x.ServicingClinician as [Servicing Provider]
			   	   ,'' as [Servicing Coded Correctly]
	   ,'' as [Points Possible2]
	   ,'' as [Actual Points3]
	--  , x.ProviderNumber 
	--  , x.PatientDOB
	--, x.Gender
	--, x. MRN
from A x
	LEFT JOIN dbo.ihsi_codingentries as co on x.PatientNumber = co.PatientNumber
	LEFT JOIN dbo.Staff as st on st.Number = x.CoderNumber 
	left join Provider as p on x.ProviderNumber = p.Number
order by p.Number asc, x.PatientNumber, co.ServiceItemCode desc
        
        






end





else-----------Clinical code--------------------------

begin




----Query to Identify Accounts for Audit

---Note:  The end DOS parameter should be set one day ahead of the range you're trying to capture since it's a "less than" sign and not "less than or equal to" 
---Note:  Modified by MAU 2014-03-17 by adding a coding entry filter to only pick up coding entries referenced in the first table
---			Added s.Userfloat2 as CodingEntryID to the temp table query and x.CodingEntryID = co.CodingEntry_ID to the query join



SET @SequenceSeed = '0000' --Increment for each new batch

SET @Auditcount = (select cast(count(distinct cdr.Summary_ID) * .02 as int)	--Calculate 2 percent of charts coded in the previous month
from IHSI_ClinCodingEntries cce
join IHSI_ClinDetailRecord cdr
	on cce.Detail_ID = cdr.Detail_ID
join Service s
	on cce.ServiceNumber = s.Number
where cce.CodedDate >= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0)
	and cce.CodedDate < DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1)
	and s.Amount * s.Units > 0.01)	
;
WITH A (PatientNumber, HID, DOS, CoderNumber, CPT, PayorNumber, PayorTypeCode, PayorTypeDescription, PayorName, Billing, BillingClinician, Servicing, ServicingClinician, ProviderNumber, CodingEntryID, DOB, Gender, MRN, Patient, UserFloat1)
AS
(
select top (@AuditCount)--to select the quantity desired, change this number.
	   s.PatientNumber
      , p.aliasname as HID
      , s.fromdate as DOS 
      , co.codedbyid as CoderNumber
      , s.serviceitemcode as CPT
	  , s.PayorNumber
	  , pay.TypeCode as PayorTypeCode
	  , pt.Description as PayorTypeDescription
	  , pay.Name as PayorName
               , s.performedbyid as Billing
               , c.Name as BillingClinician
	  , a.AttendingClinicianID as Servicing 
	  , c2.Name as ServicingClinician
	   --,s.comment as Comment
	  --, co.codeddate
	  , s.ProviderNumber 
	  , s.Userfloat2 as CodingEntryID -- (Added 03/17/2014 by MAU to filter out coding entries)
	,p.Birthdate as DOB
	,p.sex as Gender
	,p.medicalrecordno as MRN
	,p.name as Patient
	, s.UserFloat1
--into #tmpAudit
from dbo.service as s
      LEFT JOIN dbo.ihsi_clincodingentries as co ON s.number = co.servicenumber
      LEFT JOIN dbo.patient as p on p.number = co.patientnumber
      LEFT JOIN dbo.Payor as pay on pay.number = s.payornumber
      LEFT JOIN dbo.IHSI_PayorType as PT on pt.code = pay.typecode
      LEFT JOIN dbo.Admission a on a.number = s.admissionnumber
      LEFT JOIN dbo.clinician c on c.number = s.performedbyid  --Billing Clinician
      LEFT JOIN dbo.clinician c2 on c2.number = a.AttendingClinicianID  --Servicing Clinician
where s.patientnumber = p.number 
     --and co.codedbyid like '%1008'  --Coder number.
     --and s.performedbyid = '0000028'  --Billing
     --and a.attendingclinicianid  = '0000028'  --Servicing
      and co.CodedDate >= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) --First day of previous month
	  and co.CodedDate < DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) --Last Day of previous month  --Date of service range.
     --and s.serviceitemcode in ('99281','99282','99283','99284','99285','99291') -- CPT selection.
    --and s.serviceitemcode >= '99201' and s.serviceitemcode <='99291' --CPT selection range
     --and s.serviceitemcode in ('90792') --specific CPT
      --and co.codeddate >= '2018-10-01' and co.CodedDate < '2018-11-01' --Coded date
	  --and s.payornumber in ('0000001','0000003') -- Specific payor selection
	 --and s.payornumber in (select Number from dbo.Payor where typecode in ('B','C','D','F','G','H','J','K')) -- payor type selection.
	 --and s.ProviderNumber = '0000001'  --To isolate a specific provider --added on 9/17/12 per SR 8115
	 and s.Amount * s.Units > 0
order by newid() --to randomize results.
)

select @SequenceSeed + DENSE_RANK() OVER (order by x.PatientNumber) as No
	, '' as Auditor
	--, '' as PeriodPlaceholder
	, p.OrganizationName as Facility
	, x.PatientNumber as Account
      , x.HID
      , x.DOS 
      --, x.CoderNumber 
      , st.Name as Coder
      , co.ServiceItemCode as CPT
      , co.Modifier1 as Modifier
	  ,'' as [Auditor E&M Review]
	  ,'' as [Auditor CPT, Modifiers MIPS]
	  ,'' as [Auditor Comments]
	  ,'' as [Points Possible]
	  ,'' as [Actual Points]
      , co.ICDTen1 as DX1
      , co.ICDTen2 as DX2
      , co.ICDTen3 as DX3
      , co.ICDTen4 as DX4
	  ,'' as [Auditor DX]
	  ,'' as [Auditor Comments3]
	  ,'' as [Points Possible4]
	  ,'' as [Actual Points5]
      --, co.CodingEntry_ID
      --, co.Detail_ID
	  --, x.PayorNumber
	  --, x.PayorTypeCode
	  , x.PayorTypeDescription
	  , x.PayorName
	  --, x.Billing
        , x.BillingClinician as [Billing Provider]
		,'' as [BILLING Coded Correctly?]
		,'' as [Points Possible]
		,'' as [Actual Points2]
	  --, x.Servicing 
       , x.ServicingClinician as [Servicing Provider]
	   ,'' as [Servicing Coded Correctly]
	   ,'' as [Points Possible2]
	   ,'' as [Actual Points3]
	--  , x.ProviderNumber 
	--, x.DOB
	--, x.Gender
	--, x.MRN
	--, x.Patient
from A x
	LEFT JOIN dbo.IHSI_ClinCodingEntries as co on x.PatientNumber = co.PatientNumber --AND x.CodingEntryID = co.CodingEntry_ID -- (Second part of join Added 03/17/2014 by MAU to filter out coding entries)
		and co.Detail_ID = x.userfloat1
	LEFT JOIN dbo.Staff as st on st.Number = x.CoderNumber 
	LEFT JOIN dbo.Provider as p on x.ProviderNumber = p.Number
order by p.number asc, x.PatientNumber, x.dos, co.ServiceItemCode desc
        

end     





